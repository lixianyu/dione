/* OTA example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_partition.h"

#include "nvs.h"
#include "nvs_flash.h"
#include "Dione_event.h"
#include "Dione_config.h"
#include "mqtt.h"

extern mqtt_client *g_mqtt_client;
extern uint8_t gDioneState;
extern char gDioneOTAHost[64];//OTA server address
extern uint32_t gDioneOTAPort;
extern char gDioneOTAFileName[64];

#if 0
#define EXAMPLE_SERVER_IP   "www.huoweiyi.com"
//#define EXAMPLE_SERVER_IP   "112.126.75.148"
//#define EXAMPLE_SERVER_IP   "192.168.1.10"
#define EXAMPLE_SERVER_PORT "8071"
#define EXAMPLE_FILENAME "/Dione.bin"
#endif

#define BUFFSIZE 1024
#define TEXT_BUFFSIZE BUFFSIZE

static const char *TAG = "ota_dione";
/*an ota data write buffer ready to write to the flash*/
static char dione_ota_write_data[BUFFSIZE + 1] = { 0 };
/*an packet receive buffer*/
static char dione_text[TEXT_BUFFSIZE + 1] = { 0 };
/* an image total length*/
static int dione_binary_file_length = 0;
/*socket id*/
static int dione_socket_id = -1;
static char ota_http_request[64] = {0};


static void __attribute__((noreturn)) task_fatal_error()
{
    ESP_LOGE(TAG, "Exiting task due to fatal error...");
    close(dione_socket_id);
    (void)vTaskDelete(NULL);

    while (1) {
        ;
    }
}

/*read buffer by byte still delim ,return read bytes counts*/
static int read_until(char *buffer, char delim, int len)
{
//  /*TODO: delim check,buffer check,further: do an buffer length limited*/
    int i = 0;
    while (buffer[i] != delim && i < len) {
        ++i;
    }
    return i + 1;
}

static int resolve_dns(const char *host, struct sockaddr_in *ip) {
    struct hostent *he;
    struct in_addr **addr_list;
    he = gethostbyname(host);
    if (he == NULL) return 0;
    addr_list = (struct in_addr **)he->h_addr_list;
    if (addr_list[0] == NULL) return 0;
    ip->sin_family = AF_INET;
    memcpy(&ip->sin_addr, addr_list[0], sizeof(ip->sin_addr));
    return 1;
}

/* resolve a packet from http socket
 * return true if packet including \r\n\r\n that means http packet header finished,start to receive packet body
 * otherwise return false
 * */
static bool read_past_http_header(char text[], int total_len, esp_ota_handle_t update_handle)
{
    /* i means current position */
    int i = 0, i_read_len = 0;
    while (text[i] != 0 && i < total_len) {
        i_read_len = read_until(&text[i], '\n', total_len);
        // if we resolve \r\n line,we think packet header is finished
        if (i_read_len == 2) {
            int i_write_len = total_len - (i + 2);
            memset(dione_ota_write_data, 0, BUFFSIZE);
            /*copy first http packet body to write buffer*/
            memcpy(dione_ota_write_data, &(text[i + 2]), i_write_len);

            esp_err_t err = esp_ota_write( update_handle, (const void *)dione_ota_write_data, i_write_len);
            if (err != ESP_OK) {
                ESP_LOGE(TAG, "Error: esp_ota_write failed! err=0x%x", err);
                return false;
            } else {
                ESP_LOGI(TAG, "esp_ota_write header OK");
                dione_binary_file_length += i_write_len;
            }
            return true;
        }
        i += i_read_len;
    }
    return false;
}

static bool dione_connect_to_http_server()
{
    ESP_LOGI(TAG, "Server IP: %s Server Port:%d", gDioneOTAHost, gDioneOTAPort);
    sprintf(ota_http_request, "GET %s HTTP/1.1\r\nHost: %s:%d \r\n\r\n", gDioneOTAFileName, gDioneOTAHost, gDioneOTAPort);

    int  http_connect_flag = -1;
    struct sockaddr_in sock_info;
    while (1)
    {
        bzero(&sock_info, sizeof(struct sockaddr_in));
        sock_info.sin_family = AF_INET;
        //sock_info.sin_port = htons(atoi(EXAMPLE_SERVER_PORT));
        sock_info.sin_port = htons(gDioneOTAPort);

        //if host is not ip address, resolve it
        if (inet_aton( gDioneOTAHost, &(sock_info.sin_addr)) == 0) {
            ESP_LOGI(TAG, "Resolve dns for domain: %s", gDioneOTAHost);
            if (!resolve_dns(gDioneOTAHost, &sock_info)) {
                vTaskDelay(2000 / portTICK_RATE_MS);
                continue;
            }
            else
            {
                break;
            }
        }
    }
        
    dione_socket_id = socket(AF_INET, SOCK_STREAM, 0);
    if (dione_socket_id == -1) {
        ESP_LOGE(TAG, "Create socket failed!");
        return false;
    }
#if 0
    // set connect info
    memset(&sock_info, 0, sizeof(struct sockaddr_in));
    sock_info.sin_family = AF_INET;
    sock_info.sin_addr.s_addr = inet_addr(gDioneOTAHost);
    sock_info.sin_port = htons(atoi(EXAMPLE_SERVER_PORT));
#endif
    // connect to http server
    http_connect_flag = connect(dione_socket_id, (struct sockaddr *)&sock_info, sizeof(sock_info));
    if (http_connect_flag == -1) {
        ESP_LOGE(TAG, "Connect to server failed! errno=%d", errno);
        close(dione_socket_id);
        return false;
    } else {
        ESP_LOGI(TAG, "Connected to server");
        return true;
    }
    return false;
}

static void dione_ota_led_flash(void)
{
    static uint32_t k = 0;
    static uint8_t cur_led = 0;
    if (k++ % 7 == 0)
    {
        switch (cur_led)
        {
            case 0:
                gpio_set_level(LED_UWB, LED_OFF);
                gpio_set_level(LED_POWER, LED_ON);
                cur_led = 1;
                break;
            case 1:
                gpio_set_level(LED_POWER, LED_OFF);
                gpio_set_level(LED_WIFI, LED_ON);
                cur_led = 2;
                break;
            case 2:
                gpio_set_level(LED_WIFI, LED_OFF);
                gpio_set_level(LED_BLE, LED_ON);
                cur_led = 3;
                break;
            case 3:
                gpio_set_level(LED_BLE, LED_OFF);
                gpio_set_level(LED_UWB, LED_ON);
                cur_led = 0;
                break;
        }
    }
}

static void dione_ota_task(void *pvParameter)
{
    esp_err_t err;
    /* update handle : set by esp_ota_begin(), must be freed via esp_ota_end() */
    esp_ota_handle_t update_handle = 0 ;
    const esp_partition_t *update_partition = NULL;

    ESP_LOGE(TAG, "Enter %s, wait ota MQTT command...", __func__);
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_OTA_START,
                        false, false, portMAX_DELAY); // portMAX_DELAY : 497.102696 days
    ESP_LOGI(TAG, "Start to Connect to Server....");
    char mqtt_buf[64] = {0};

    strcpy(mqtt_buf, "Start to Connect to OTA Server....");
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strlen(mqtt_buf), 0, 0);
    
    gDioneState = 1;
    gpio_set_level(LED_UWB, LED_OFF);
    gpio_set_level(LED_BLE, LED_OFF);
    gpio_set_level(LED_WIFI, LED_OFF);
    const esp_partition_t *configured = esp_ota_get_boot_partition();
    const esp_partition_t *running = esp_ota_get_running_partition();

    assert(configured == running); /* fresh from reset, should be running from configured boot partition */
    ESP_LOGI(TAG, "Running partition type %d subtype %d (offset 0x%08x)",
             configured->type, configured->subtype, configured->address);

    
    /*connect to http server*/
    if (dione_connect_to_http_server()) {
        strcpy(mqtt_buf, "Connected to http server");
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strlen(mqtt_buf), 0, 0);
        ESP_LOGI(TAG, "Connected to http server");
    } else {
        strcpy(mqtt_buf, "Connect to http server failed!");
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strlen(mqtt_buf), 0, 0);
        ESP_LOGE(TAG, "Connect to http server failed!");
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        task_fatal_error();
    }

    int res = -1;
    /*send GET request to http server*/
    #if 0
    res = send(dione_socket_id, ota_http_request, strlen(ota_http_request), 0);
    #else
    res = write(dione_socket_id, ota_http_request, strlen(ota_http_request));
    #endif
    if (res == -1) {
        ESP_LOGE(TAG, "Send GET request to server failed");
        task_fatal_error();
    } else {
        ESP_LOGI(TAG, "Send GET request to server succeeded");
    }

    update_partition = esp_ota_get_next_update_partition(NULL);
    ESP_LOGI(TAG, "Writing to partition subtype %d at offset 0x%x",
             update_partition->subtype, update_partition->address);
    assert(update_partition != NULL);

    err = esp_ota_begin(update_partition, OTA_SIZE_UNKNOWN, &update_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "esp_ota_begin failed, error=%d", err);
        task_fatal_error();
    }
    ESP_LOGI(TAG, "esp_ota_begin succeeded");

    bool resp_body_start = false, flag = true;
    uint32_t co = 1;
    //uint32_t level = 1;
    /*deal with all receive packet*/
    while (flag) {
        #if 0
        gpio_set_level(LED_POWER, level);
        gpio_set_level(LED_UWB, level);
        gpio_set_level(LED_WIFI, level);
        gpio_set_level(LED_BLE, level);
        #else
        dione_ota_led_flash();
        #endif
        vTaskDelay(5);
        if (co++ % 10 == 0)
        {
            sprintf(mqtt_buf, "Update %d bytes", dione_binary_file_length);
            mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strlen(mqtt_buf), 0, 0);
        }
        
        memset(dione_text, 0, TEXT_BUFFSIZE);
        memset(dione_ota_write_data, 0, BUFFSIZE);
        #if 0
        int buff_len = recv(dione_socket_id, dione_text, TEXT_BUFFSIZE, 0);
        #else
        int buff_len = read(dione_socket_id, dione_text, TEXT_BUFFSIZE);
        #endif
        if (buff_len < 0) { /*receive error*/
            ESP_LOGE(TAG, "Error: receive data error! errno=%d", errno);
            task_fatal_error();
        } else if (buff_len > 0 && !resp_body_start) { /*deal with response header*/
            memcpy(dione_ota_write_data, dione_text, buff_len);
            resp_body_start = read_past_http_header(dione_text, buff_len, update_handle);
        } else if (buff_len > 0 && resp_body_start) { /*deal with response body*/
            memcpy(dione_ota_write_data, dione_text, buff_len);
            err = esp_ota_write( update_handle, (const void *)dione_ota_write_data, buff_len);
            if (err != ESP_OK) {
                ESP_LOGE(TAG, "Error: esp_ota_write failed! err=0x%x", err);
                task_fatal_error();
            }
            dione_binary_file_length += buff_len;
            ESP_LOGI(TAG, "Have written image length %d", dione_binary_file_length);
        } else if (buff_len == 0) {  /*packet over*/
            flag = false;
            ESP_LOGI(TAG, "Connection closed, all packets received");
            close(dione_socket_id);
        } else {
            ESP_LOGE(TAG, "Unexpected recv result");
        }
        //level = !level;
    }

    ESP_LOGI(TAG, "Total Write binary data length : %d", dione_binary_file_length);
    strcpy(mqtt_buf, "Checking OTA..");
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strnlen(mqtt_buf, sizeof(mqtt_buf)), 0, 0);
    if (esp_ota_end(update_handle) != ESP_OK) {
        ESP_LOGE(TAG, "esp_ota_end failed!");
        strcpy(mqtt_buf, "esp_ota_end failed!");
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strnlen(mqtt_buf, sizeof(mqtt_buf)), 0, 0);
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        task_fatal_error();
    }

    strcpy(mqtt_buf, "Checking OTA......");
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strnlen(mqtt_buf, sizeof(mqtt_buf)), 0, 0);
    err = esp_ota_set_boot_partition(update_partition);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "esp_ota_set_boot_partition failed! err=0x%x", err);
        sprintf(mqtt_buf, "esp_ota_set_boot_partition failed! err=0x%x", err);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strnlen(mqtt_buf, sizeof(mqtt_buf)), 0, 0);
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        task_fatal_error();
    }
    sprintf(mqtt_buf, "OTA sucessed!(%d), system are going to restart...", dione_binary_file_length);
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, mqtt_buf, strnlen(mqtt_buf, sizeof(mqtt_buf)), 0, 0);
    ESP_LOGI(TAG, "Prepare to restart system!");
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    esp_restart();
    return ;
}

void dione_ota_init(void)
{
    xTaskCreate(dione_ota_task, "OTA", 8192, NULL, 9, NULL);
}
