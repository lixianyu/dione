#ifndef __DIONE_CONFIG_LOCAL_H__
#define __DIONE_CONFIG_LOCAL_H__

#define DIONE_SW_VERSION "0.0.A17.03"

//#define BLE_LED_IN_CALLBACK

//#define MQTT_SERVER_DIONE
//#define MQTT_SERVER_ONENET
//#define MQTT_SERVER_MICRODUINO
#define MQTT_SERVER_SH_HUOWEI


#define WIFI_SSID "TaiYangGong2.4G"
#define WIFI_PASS "6hy5BIktTss"
//#define WIFI_PASS "9hy5BIktT"
//#define CPU_FREQ_160MHZ

#define LED_ON    0
#define LED_OFF   1

#define UART_BUF_SIZE (1024)
// For UART (UWB)
#define DIONE_UART_TXD (GPIO_NUM_16)
#define DIONE_UART_RXD (GPIO_NUM_17)

// For trace
/*
 TXD0 to GPIO1
 RXD0 to GPIO3
 */
// For LEDs
#define LED_POWER GPIO_NUM_15 // Red
#define LED_UWB   GPIO_NUM_5 // Green
#define LED_WIFI  GPIO_NUM_12  // Yellow
#define LED_BLE   GPIO_NUM_2 // Blue
#define LED_SHAKE LED_POWER

// For Ethernet
#define PIN_SMI_MDC   GPIO_NUM_23
#define PIN_SMI_MDIO  GPIO_NUM_18
/* phy_rmii_configure_data_interface_pins
   CRS_DRV to GPIO27
   TXD0 to GPIO19
   TX_EN to GPIO21
   TXD1 to GPIO22
   RXD0 to GPIO25
   RXD1 to GPIO26
   RMII CLK to GPIO0
 */

// For SPI Flash
/*
 SPIHD to GPIO9
 SPIWP to GPIO10
 SPICS0 to GPIO11
 SPICLK to GPIO6
 SPIQ to GPIO7
 SPID to GPIO8
 */

// For I2C
#define DIONE_I2C_SCL GPIO_NUM_4
#define DIONE_I2C_SDA GPIO_NUM_13


#define DIONE_POWER_ADC GPIO_NUM_35

#define DIONE_SHAKE_PIN GPIO_NUM_14
// Not used
// GPIO32, GPIO33

#define VOLTAGE_5V_ADC_VALUE 3419


#define CONFIG_PHY_LAN8720
#define CONFIG_PHY_ADDRESS 1
#define CONFIG_PHY_USE_POWER_PIN

#ifdef MQTT_SERVER_SH_HUOWEI
#define MQTT_PUB_TOPIC_CONTROL "/queue/dione.up"
#define MQTT_PUB_TOPIC_DATA "/queue/dione.ibeacon"
#define MQTT_PUB_TOPIC_UWB "/queue/uwb"
#define MQTT_SUB_TOPIC "/topic/dione.down"
#else
#define MQTT_PUB_TOPIC ""
#define MQTT_SUB_TOPIC "Dione/command"
#endif

#define MQTT_UP_MSG_TYPE_0 0x0000 // 设备登录与心跳
#define MQTT_UP_MSG_TYPE_1 0x0001 // 设备基本配置信息上报
#define MQTT_UP_MSG_TYPE_2 0x0002 // 设备WIFI配置信息上报
#define MQTT_UP_MSG_TYPE_4 0x0004 // 设备iBeacon信息上报

#define MQTT_DOWN_MSG_TYPE_0 0x1000 // 控制设备信息上传
#define MQTT_DOWN_MSG_TYPE_1 0x1001 // 推送设备升级
#define MQTT_DOWN_MSG_TYPE_2 0x1002 // 推送设备WIFI配置
#define MQTT_DOWN_MSG_TYPE_4 0x1004 // 推送设备iBeacon配置
#endif

