// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_partition.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "driver/uart.h"
#include "driver/adc.h"
#include "soc/uart_struct.h"

#include "bt.h"
#include "bt_trace.h"
#include "bt_types.h"
#include "btm_api.h"
#include "bta_api.h"
#include "bta_gatt_api.h"

#include "esp_gatt_defs.h"
#include "esp_gatts_api.h"
#include "esp_gattc_api.h"

#include "esp_blufi_api.h"
#include "esp_bt_defs.h"
#include "esp_gap_ble_api.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "Dione.h"
#include "Dione_event.h"
#include "Dione_config.h"
#include "Dione_task_priority.h"
#include "mqtt.h"
#ifdef SECOND_MQTT_SERVER
#include "dione_mqtt.h"
#endif
#include "apps/sntp/sntp.h"
//#include "dis_api.h"
#include "dione_profile.h"
#include "dione_fun.h"

#include "esp_vfs.h"
#include "esp_vfs_fat.h"
#include "jsmn.h"
//#include "aws-iot-device-sdk-embedded-C\aws_iot_json_utils.h"
#include "aws_iot_json_utils.h"
#include "aws_iot_shadow_json.h"

extern void dione_ota_init(void);
extern void dione_interrupt_init(void);
extern void dione_initialise_ethernet(void);
extern void dione_uninitialise_ethernet(void);

static void gattc_profile_a_event_handler(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param);
static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);
static void dione_blufi_event_callback(esp_blufi_cb_event_t event, esp_blufi_cb_param_t *param);
static void resolve_command(char *msg);
static void getTime(void *pvParameters);
esp_err_t dione_nvs_save_bleServer(uint8_t ble);
static esp_err_t dione_nvs_save_wifi_mode(void);
static esp_err_t dione_nvs_save_ap_channel(void);
static esp_err_t dione_nvs_save_ap_auth_mode(void);
static esp_err_t dione_nvs_save_ap_max_conn_num(void);
static esp_err_t dione_nvs_save_ap_pass(void);
static esp_err_t dione_nvs_save_ap_ssid(void);
static esp_err_t dione_nvs_save_sta_pass(void);
static esp_err_t dione_nvs_save_sta_ssid(void);
static esp_err_t dione_nvs_save_manual_restart(void);
static void dione_mqtt_save_ibeacon_task(void *pvParameters);
esp_err_t dione_nvs_save_seq(void);
static void dione_wifi_promiscuous(void *buf, wifi_promiscuous_pkt_type_t type);

#define DIONE_STORAGE_NAMESPACE "storage"

#define GATTS_SERVICE_UUID_TEST_A   0xFFE0
#define GATTS_CHAR_UUID_TEST_A      0xFFE1
#define GATTS_CHAR2_UUID_TEST_A      0xFFE2
#define GATTS_DESCR_UUID_TEST_A     0x3333
#define GATTS_NUM_HANDLE_TEST_A     4

#define GATTS_SERVICE_UUID_TEST_B   0x00EE
#define GATTS_CHAR_UUID_TEST_B      0xEE01
#define GATTS_DESCR_UUID_TEST_B     0x2222
#define GATTS_NUM_HANDLE_TEST_B     4

#define GATTS_TAG "GATTS_DEMO"
#define GATTC_TAG "GATTC_DEMO"
static const char device_name[] = "Alert Notification";
static bool connect = false;
static esp_ble_scan_params_t ble_scan_params = {
    .scan_type              = BLE_SCAN_TYPE_ACTIVE,
    .own_addr_type          = BLE_ADDR_TYPE_PUBLIC,
    .scan_filter_policy     = BLE_SCAN_FILTER_ALLOW_ALL,
    .scan_interval          = 1280, // 800ms
    .scan_window            = 800   // 500ms
};

static esp_gatt_srvc_id_t alert_service_id = {
    .id = {
        .uuid = {
            .len = ESP_UUID_LEN_16,
            .uuid = {.uuid16 = 0x1811,},
        },
        .inst_id = 0,
    },
    .is_primary = true,
};

static esp_gatt_id_t notify_descr_id = {
    .uuid = {
        .len = ESP_UUID_LEN_16,
        .uuid = {.uuid16 = GATT_UUID_CHAR_CLIENT_CONFIG,},
    },
    .inst_id = 0,
};
uint32_t gScanTime = 0; // 记录此次扫描到广播消息的系统运行时间 (ms)
bool gbIfEverScanAdv = false; // 开机后是否曾经扫描到广播消息
char *TAG_DIONE = "Dione";
uint8_t gDioneState = 0; // 1, OTAing
TaskHandle_t gDioneUARTTH = NULL;
TaskHandle_t gDioneBLE = NULL;
TaskHandle_t gDioneHeartBeat = NULL;
TaskHandle_t gDioneAdvScan = NULL;
TaskHandle_t gDioneTemp = NULL;
#define GATTS_DEMO_CHAR_VAL_LEN_MAX 0x40
static uint8_t char1_str[] = {0x11,0x22,0x33};
static uint8_t char2_str[20];
static esp_attr_value_t gatts_demo_char1_val = 
{
    .attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
    .attr_len     = sizeof(char1_str),
    .attr_value   = char1_str,
};
static esp_attr_value_t gatts_demo_char2_val = 
{
    .attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
    .attr_len     = sizeof(char2_str),
    .attr_value   = char2_str,
};

#ifdef CONFIG_SET_RAW_ADV_DATA
/* For iBeacon adver data:
# Actual Advertising Data Starts Here
02 01 1a
1a ff 4c 00 02 15 # Apple's static prefix to the advertising data -- this is always the same
e2 c5 6d b5 df fb 48 d2 b0 60 d0 f5 a7 10 96 e0 # iBeacon profileUUID
00 00 # major (LSB first)
00 00 # minor (LSB first)
c5 # The 2's complement of the calibrated Tx Power
*/
uint8_t dione_raw_adv_data[] =
{
    0x02,   // length of first data structure (2 bytes excluding length byte)
    ESP_BLE_AD_TYPE_FLAG,   // AD Type = Flags
    ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT,

    // service UUID, to notify central devices what services are included
    // in this peripheral
    0x1B,   // length of second data structure (7 bytes excluding length byte)
    0xff, 0x4c, 0x00, 0x02, 0x15,
    0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25,
    //0xe2, 0xc5, 0x6d, 0xb5, 0xdf, 0xfb, 0x48, 0xd2, 0xb0, 0x60, 0xd0, 0xf5, 0xa7, 0x10, 0x96, 0xe0,
#if 0
    0x00, 0x0a,
    0x00, 0x07,
#elif 1
    0x27, 0x11,
    0xE9, 0xDD,
#else
    0x27, 0x11, //10001
    0x09, 0xE0, //59872
#endif
    0xc5,
    0xBB,
};
uint8_t dione_raw_scan_rsp_data[] =
{
    0x0E,   // length of this data
    ESP_BLE_AD_TYPE_NAME_CMPL,
    0x44,   // 'D'
    0x69,   // 'i'
    0x6F,   // 'o'
    0x6E,   // 'n'
    0x65,   // 'e'
    1, 2, 3, 4,
    5, 6, 7, 8,

    0x02,   // length of this data
    ESP_BLE_AD_TYPE_TX_PWR, //TX Power Level
    0       // 0dBm
};
#else
static uint8_t dione_service_uuid128[32] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00,
};

//static uint8_t test_manufacturer[TEST_MANUFACTURER_DATA_LEN] =  {0x12, 0x23, 0x45, 0x56};
static esp_ble_adv_data_t dione_adv_data = {
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = true,
    .min_interval = 0x100,
    .max_interval = 0x100,
    .appearance = 0x00,
    .manufacturer_len = 0,
    .p_manufacturer_data =  NULL,
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = 16,
    .p_service_uuid = dione_service_uuid128,
    .flag = 0x6,
};
#endif
uint16_t g_intervalADV = 160; // 1280 * 0.625 = 800ms
esp_ble_adv_params_t dione_adv_params = {
    .adv_int_min        = 160, // 1280 * 0.625 = 800ms, 32 * 0.625 = 20ms
    .adv_int_max        = 160,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};

#define WIFI_LIST_NUM   10

static wifi_config_t sta_config;
static wifi_config_t ap_config;
static uint8_t gSTA_DISCONNECTED_counts = 0;
static uint8_t gSurfMode = 0; // 0, auto; 1, To use wifi only; 2, To use Ethernet only to surf the Internet.
static bool gIfGotIP = false;
/* store the station info for send back to phone */
static bool gl_sta_connected = false;
static uint8_t gl_sta_bssid[6];
static uint8_t gl_sta_ssid[32];
static int gl_sta_ssid_len;
//#define DIONE_DEVICE_NAME            "Dione"
// The following value will be saved into NVS.
char gBLEDeviceName[21] = "Dione";
uint8_t gBLEServer = 0; // 0, is BLE client; 1, is BLE server; 2, client&server

static uint8_t gMajor[2] = {0x27, 0x11};
static uint8_t gMinor[2] = {0xE9, 0xDD};
static uint8_t gUUID[16] = {0xFD, 0xA5, 0x06, 0x93, 0xA4, 0xE2, 0x4F, 0xB1, 0xAF, 0xCF, 0xC6, 0xEB, 0x07, 0x64, 0x78, 0x25};
static uint8_t gTxPowerCalibrated = 0xC5;
uint16_t g_scan_interval = 1280;// 1280 * 0.625 = 800ms
uint16_t g_scan_window = 160; // 160 * 0.625 = 100ms
static uint8_t gWifiMode = WIFI_MODE_STA;
static uint8_t gSTA_bssid[6];//MAC address of target AP
static uint8_t gSTA_ssid[32];
static uint8_t gSTA_pass[64];
static uint8_t gAP_ssid[32];
static uint8_t gAP_pass[64];
static uint8_t gAP_max_connection = 4;
static uint8_t gAP_auth_mode = WIFI_AUTH_WPA_WPA2_PSK;
static int8_t gAP_channel = -1;
uint8_t gManualRestart = 0; // 1, It is manual reset. 0, something was wrong reset.
char gDioneOTAHost[64];//OTA server address
uint32_t gDioneOTAPort = 8071;
char gDioneOTAFileName[64];

char gDioneHost[64];//MQTT server address
#ifdef MQTT_SERVER_ONENET
uint32_t gDionePort = 6002;//MQTT server port
#elif defined(MQTT_SERVER_DIONE)
uint32_t gDionePort = 1883;//MQTT server port
#elif defined(MQTT_SERVER_MICRODUINO)
uint32_t gDionePort = 1883;//MQTT server port
#elif defined(MQTT_SERVER_SH_HUOWEI)
uint32_t gDionePort = 1883;//MQTT server port
#endif

#ifdef MQTT_SERVER_SH_HUOWEI
char gCID[16];
uint64_t gSeq = 0;
#endif

uint64_t gShakeCount = 0;
int gADCvalue = VOLTAGE_5V_ADC_VALUE;
float gVoltage;

char gUserKEY[32];
uint16_t gHeartInterval = 10; // second

uint8_t gHaveUM100 = 0;// 1, UM100 exist; 0, There is no UM100
static uint8_t gUWBDeviceType = 1;// 1, Anchor; 2, Observer; 3, Tag
/* 适用场景
0：默认值（默认为3）
1：长距离、低速
2：中距离、中低速
3：中距离、中高速
 */
static uint8_t gUWBShiYongChangJing = 3;
/* UWB 信道号
0：默认值（默认为2）
1：信道1（3244.8 - 3744 MHz）
2：信道2（3774 - 4243.2 MHz）
3：信道3（4243.2 – 4742.4 MHz）
4：信道4（3328 – 4659.2 MHz）
 */
static uint8_t gUWBChannel = 2;
/* 扩频码ID
0：默认值（默认与UWB信道号相同）
1：扩频码0
2：扩频码1
3：扩频码2
4：扩频码3
 */
static uint8_t gUWBKuoPinMa = 2;
/* 单次测距时追踪信标的最大数量
    0：默认值（默认为8）
    其它有效值：对应数值
 */
static uint8_t gUWBHowMany = 8;
/* 每个测距轮中连续测距次数
    0：默认值（默认为1）
    其它有效值：对应数值
 */
static uint16_t gUWBTimes = 1;
/* 
 标签相邻两轮测距之间的间隔时间（单位：毫秒）
（该参数只对标签设备有效）
    0：默认值（默认为50毫秒）
    其它有效值：数值对应毫秒数
 */
static uint16_t gUWBInterval = 50;
char gDapsCommand[32] = {0};
static int32_t g_restart_counter;

mqtt_client *g_mqtt_client = NULL;
#ifdef SECOND_MQTT_SERVER
dione_mqtt_client *g_mqtt_client2 = NULL;
#endif
bool gMQTTConnected = false;
bool gJB = false;

static void dione_close_uwb_led_task(void *pvParameters)
{
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    gpio_set_level(LED_UWB, LED_OFF);
    vTaskDelete(NULL);
}

static void connected_cb(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
#ifdef MQTT_SERVER_DIONE
    mqtt_client *client = (mqtt_client *)self;
    mqtt_subscribe(client, MQTT_SUB_TOPIC, 0);
#elif defined(MQTT_SERVER_ONENET)
#elif defined(MQTT_SERVER_MICRODUINO)
#elif defined(MQTT_SERVER_SH_HUOWEI)
    mqtt_client *client = (mqtt_client *)self;
    mqtt_subscribe(client, MQTT_SUB_TOPIC, 0);
#endif
    //mqtt_publish(client, "Dione/UM100", "howdy!", 6, 0, 0);
    xEventGroupSetBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED);
    //ESP_ERROR_CHECK(esp_wifi_set_promiscuous(true));
    gpio_set_direction(LED_WIFI, GPIO_MODE_OUTPUT);
    esp_wifi_set_promiscuous(true);
    xTaskCreate(dione_close_uwb_led_task, "close_uwb_led", 2048, NULL, 4, NULL);
    gMQTTConnected = true;
}

static void reconnect_cb(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
}

static void subscribe_cb(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
#if 0
    mqtt_info("[APP] Subscribe ok, test publish msg\n");
    mqtt_client *client = (mqtt_client *)self;
    mqtt_publish(client, "Dione/", "lixianyu", 8, 0, 0);
#endif
}

static void publish_cb(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
}

static void disconnected_cb(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    if (gDioneState == 1) // OTAing
    {
        esp_restart();
    }
    if (gHaveUM100 == 1)
    {
        char data[32] = {0};
        uart_flush(UART_NUM_1);
        //vTaskDelay(100/portTICK_PERIOD_MS);
        strncpy((char*)data, "daps\r\n", 6);
        uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        vTaskDelay(100/portTICK_PERIOD_MS);
        uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        vTaskDelay(100/portTICK_PERIOD_MS);
    }
    vTaskDelay(3000/portTICK_PERIOD_MS);
    esp_restart();
}

#ifdef SECOND_MQTT_SERVER
static void connected_cb2(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    dione_mqtt_client *client = (dione_mqtt_client *)self;
    xEventGroupSetBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED2);
    gMQTTConnected = true;
}

static void reconnect_cb2(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
}

static void subscribe_cb2(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
#if 0
    mqtt_info("[APP] Subscribe ok, test publish msg\n");
    mqtt_client *client = (mqtt_client *)self;
    mqtt_publish(client, "Dione/", "lixianyu", 8, 0, 0);
#endif
}

static void publish_cb2(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
}

static void disconnected_cb2(void *self, void *params)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    if (gDioneState == 1) // OTAing
    {
        esp_restart();
    }
    if (gHaveUM100 == 1)
    {
        char data[32] = {0};
        uart_flush(UART_NUM_1);
        //vTaskDelay(100/portTICK_PERIOD_MS);
        strncpy((char*)data, "daps\r\n", 6);
        uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        vTaskDelay(100/portTICK_PERIOD_MS);
        uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        vTaskDelay(100/portTICK_PERIOD_MS);
    }
    vTaskDelay(3000/portTICK_PERIOD_MS);
    esp_restart();
}
#endif

static void dione_mqtt_reset_task(void *pvParameters)
{
    ESP_LOGI(TAG_DIONE, "Enter %s", __func__);
    char *buf = malloc(128);
    for (int i = 5; i >= 0; i--) 
    {
        sprintf(buf, "Restarting in %d seconds...\n", i);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, buf, strlen(buf), 0, 0);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    esp_restart();
}

static bool gSuspendding = false;
static void dione_suspend_uart_task(void *pvParameters)
{
    gSuspendding = true;
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskDelay(3500 / portTICK_PERIOD_MS);
    vTaskSuspend(gDioneUARTTH);
    ESP_LOGW(TAG_DIONE, "Leave %s", __func__);
    gSuspendding = false;
    gpio_set_level(LED_UWB, LED_OFF);
    vTaskDelete(NULL);
}

static void dione_begin_ota_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskSuspend(gDioneAdvScan);
    gDioneState = 1;
    dione_nvs_save_seq();
    vTaskDelay(4501 / portTICK_PERIOD_MS);
    esp_ble_gap_stop_advertising();
    esp_ble_gap_stop_scanning();
    xEventGroupSetBits(dione_event_group, DIONE_EVENT_OTA_START);
    vTaskDelete(NULL);
}

static void dione_mqtt_rsp_config_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    uint32_t interval = g_intervalADV * 0.625;
    char uuid[64] = {0};
    char temp[16] = {0};
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
    vTaskDelay(3000/portTICK_PERIOD_MS);
    //sprintf(uuid, "", );
    for (int i = 0; i < 16; i++)
    {
        sprintf(temp, "%02X", gUUID[i]);
        strcat(uuid, temp);
    }
    
    char *json_buf = (char*)malloc(1024);
    sprintf(json_buf, "{\"cid\":\"%s\",\"seq\":%llu,\"cmd\":%d,\"time\":%ld,\"msg\":{\
        \"base\":{\"romv\":\"%s\",\"mac\":\"%s\"},\
        \"wifi\":{\"ac\":{\"ssid\":\"%s\",\"pwd\":\"%s\"},\"ap\":{\"ssid\":\"%s\",\"pwd\":\"%s\"}},\
        \"ibeacon\":{\"base\":{\"interval\":%u}, \"tag\":{\"uuid\":\"%s\", \"major\":\"%X%X\", \"minor\":\"%X%X\"}}\
        }}", 
        gCID, gSeq++, MQTT_UP_MSG_TYPE_1|MQTT_UP_MSG_TYPE_2|MQTT_UP_MSG_TYPE_4, 
        time(NULL), DIONE_SW_VERSION, gCID,
        gSTA_ssid, gSTA_pass, gAP_ssid, gAP_pass,
        interval, uuid, gMajor[0], gMajor[1], gMinor[0], gMinor[1]);
    
    int len = strlen(json_buf);
    ESP_LOGE(TAG_DIONE, "The json len is %d", len);
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)json_buf, len, 0, 0);
    free(json_buf);
    vTaskDelete(NULL);
}

static void dione_mqtt_rsp_ota_task(void *pvParameters)
{
    //int tokenCount = *((int*)pvParameters);
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    char *buf = malloc(64);
    if (gBLEServer == 2)
    {
        esp_ble_gap_stop_advertising();
        esp_blufi_profile_deinit();
        esp_ble_gap_stop_scanning();
    }
    else if (gBLEServer == 1) // This is a server
    {
        esp_ble_gap_stop_advertising();
        esp_blufi_profile_deinit();
    }
    else
    {
        esp_ble_gap_stop_scanning();
    }
    esp_wifi_set_promiscuous(false);
    //blufi_security_deinit();
    if (gHaveUM100 == 1)
    {
        vTaskSuspend(gDioneUARTTH);
    }
    if (gDioneBLE != NULL) vTaskSuspend(gDioneBLE);
    if (gDioneHeartBeat != NULL) vTaskSuspend(gDioneHeartBeat);
    if (gDioneTemp != NULL) vTaskSuspend(gDioneTemp);
    strncpy(buf, "OTA start...please wait a moment.", 64);
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, buf, strlen(buf), 0, 0);
    free(buf);
    xTaskCreate(dione_begin_ota_task, "bot", 2048, NULL, 25, NULL);
    vTaskDelete(NULL);
}

static jsmn_parser json_parser;
static jsmntok_t token[128];
inline int8_t Dione_jsoneq(const char *json, jsmntok_t *tok, const char *s)
{
	if(tok->type == JSMN_STRING) {
		if((int) strlen(s) == tok->end - tok->start) {
			if(strncmp(json + tok->start, s, (size_t) (tok->end - tok->start)) == 0) {
				return 0;
			}
		}
	}
	return -1;
}

bool Dione_extractCmdNumber(const char *pJsonDocument, int32_t tokenCount, uint32_t *pNumber)
{
	int32_t i;
	IoT_Error_t ret_val = SUCCESS;

	for(i = 1; i < tokenCount; i++) {
		if(Dione_jsoneq(pJsonDocument, &(token[i]), "cmd") == 0) {
			ret_val = parseUnsignedInteger32Value(pNumber, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
				return true;
			}
		}
	}
	return false;
}

bool Dione_extractROMNameServerPort(const char *pJsonDocument, int32_t tokenCount)
{
	int32_t i;
	IoT_Error_t ret_val = SUCCESS;
    bool flagHost = false;
    bool flagName = false;
    bool flagPort = false;

	for(i = 1; i < tokenCount; i++) {
		if(Dione_jsoneq(pJsonDocument, &(token[i]), "romname") == 0) {
            ret_val = parseStringValue(gDioneOTAFileName, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
				flagName = true;
                continue;
			}
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "server") == 0) {
            ret_val = parseStringValue(gDioneOTAHost, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
				flagHost = true;
                continue;
			}
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "port") == 0) {
			ret_val = parseUnsignedInteger32Value(&gDioneOTAPort, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
                flagPort = true;
				continue;
			}
		}
	}
    if (flagHost && flagName && flagPort)
    {
        return true;
    }
	return false;
}

static void dione_mqtt_save_wifi_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);

    err = nvs_set_blob(my_handle, "dione.sta.ssid", gSTA_ssid, sizeof(gSTA_ssid));
    err = nvs_set_blob(my_handle, "dione.sta.pass", gSTA_pass, sizeof(gSTA_pass));
    err = nvs_set_blob(my_handle, "dione.ap.ssid", gAP_ssid, sizeof(gAP_ssid));
    err = nvs_set_blob(my_handle, "dione.ap.pass", gAP_pass, sizeof(gAP_pass));
    
    err = nvs_commit(my_handle);
    nvs_close(my_handle);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    vTaskDelete(NULL);
}

static bool Dione_extractACorAP(const char *pJsonDocument, int32_t tokenCount, int32_t tokenBegin, bool bACAP)
{
    IoT_Error_t ret_val = SUCCESS;
    int32_t i;
    bool flagSSID = false;
    bool flagPwd = false;
    int32_t tokenEnd = tokenBegin + tokenCount;
    ESP_LOGW(TAG_DIONE, "Enter %s, pJsonDocument=%s, tokenBegin=%d, tokenCount=%d", __func__, pJsonDocument, tokenBegin, tokenCount);
    for(i = tokenBegin; i < tokenEnd; i++) {
		if (Dione_jsoneq(pJsonDocument, &(token[i]), "ssid") == 0) {
            ESP_LOGW(TAG_DIONE, "There is ssid");
            if (bACAP)
            {
                ret_val = parseStringValue((char*)gSTA_ssid, pJsonDocument, &token[i + 1]);
            }
            else
            {
                ret_val = parseStringValue((char*)gAP_ssid, pJsonDocument, &token[i + 1]);
            }
			if(ret_val == SUCCESS) {
				flagSSID = true;
                continue;
			}
		}
        if (Dione_jsoneq(pJsonDocument, &(token[i]), "pwd") == 0) {
            ESP_LOGW(TAG_DIONE, "There is pwd");
            if (bACAP)
            {
                ret_val = parseStringValue((char*)gSTA_pass, pJsonDocument, &token[i + 1]);
            }
            else
            {
                ret_val = parseStringValue((char*)gAP_pass, pJsonDocument, &token[i + 1]);
            }
			if(ret_val == SUCCESS) {
				flagPwd = true;
                continue;
			}
		}
	}
    if (flagSSID && flagPwd)
    {
        return true;
    }
    return false;
}

bool Dione_extractWifi(const char *pJsonDocument, int32_t tokenCount)
{
	int32_t i;
	IoT_Error_t ret_val = SUCCESS;
    //bool flagWifi = false;
    bool flagAC = false;
    bool flagAP = false;
    ESP_LOGE(TAG_DIONE, "Enter %s, tokenCount=%d", __func__, tokenCount);
	for(i = 1; i < tokenCount; i++) {
		if(Dione_jsoneq(pJsonDocument, &(token[i]), "ac") == 0) {
            ESP_LOGE(TAG_DIONE, "There is ac...");
            flagAC = Dione_extractACorAP(pJsonDocument, 5, i + 1, true);
            continue;
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "ap") == 0) {
            ESP_LOGE(TAG_DIONE, "There is ap...");
            flagAP = Dione_extractACorAP(pJsonDocument, 5, i + 1, false);
            continue;
		}
	}
    if (flagAC && flagAP)
    {
        xTaskCreate(dione_mqtt_save_wifi_task, "saveWifi", 2048, NULL, 11, NULL);
        return true;
    }
	return false;
}

bool Dione_extractUUIDMajorMinorInterval(const char *pJsonDocument, int32_t tokenCount)
{
    char aBuf[64] = {0};
    uint32_t intervalADV = 800;// ms
	int32_t i;
	IoT_Error_t ret_val = SUCCESS;
    bool flagUUID = false;
    bool flagMajor = false;
    bool flagMinor = false;
    bool flagInterval = false;
    char *p = NULL;
    char ch;
    uint8_t high = 0,low = 0;
    uint16_t major,minor;

	for(i = 1; i < tokenCount; i++) {
		if(Dione_jsoneq(pJsonDocument, &(token[i]), "uuid") == 0) {
            memset(aBuf, 0, sizeof(aBuf));
            ret_val = parseStringValue(aBuf, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
				flagUUID = true;
                p = aBuf;
                for (int j = 0; j < 16; j++)
                {
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        high = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        high = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        high = (uint8_t)(ch - 'A') + 10;
                    }
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        low = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        low = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        low = (uint8_t)(ch - 'A') + 10;
                    }
                    gUUID[j] = (high<<4) | low;
                }
                memcpy(dione_raw_adv_data+9, gUUID, 16);
                continue;
			}
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "major") == 0) {
            memset(aBuf, 0, sizeof(aBuf));
            ret_val = parseStringValue(aBuf, pJsonDocument, &token[i + 1]);
            ESP_LOGE(TAG_DIONE, "aBuf major = %s", aBuf);
			if(ret_val == SUCCESS) {
				flagMajor = true;
                p = aBuf;
                for (int j = 0; j < 2; j++)
                {
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        high = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        high = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        high = (uint8_t)(ch - 'A') + 10;
                    }
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        low = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        low = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        low = (uint8_t)(ch - 'A') + 10;
                    }
                    gMajor[j] = (high<<4) | low;
                }
                //ESP_LOGE(TAG_DIONE, "major = %d", major);
                memcpy(dione_raw_adv_data+25, gMajor, 2);
                continue;
			}
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "minor") == 0) {
            memset(aBuf, 0, sizeof(aBuf));
            ret_val = parseStringValue(aBuf, pJsonDocument, &token[i + 1]);
            ESP_LOGE(TAG_DIONE, "aBuf minor = %s", aBuf);
			if(ret_val == SUCCESS) {
				flagMinor = true;
                p = aBuf;
                for (int j = 0; j < 2; j++)
                {
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        high = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        high = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        high = (uint8_t)(ch - 'A') + 10;
                    }
                    ch = *p++;
                    if (ch >= '0' && ch <= '9')
                    {
                        low = (uint8_t)(ch - '0');
                    }
                    else if (ch >= 'a' && ch <= 'z')
                    {
                        low = (uint8_t)(ch - 'a') + 10;
                    }
                    else if (ch >= 'A' && ch <= 'Z')
                    {
                        low = (uint8_t)(ch - 'A') + 10;
                    }
                    gMinor[j] = (high<<4) | low;
                }
                memcpy(dione_raw_adv_data+27, gMinor, 2);
                continue;
			}
		}
        if(Dione_jsoneq(pJsonDocument, &(token[i]), "interval") == 0) {
			ret_val = parseUnsignedInteger32Value(&intervalADV, pJsonDocument, &token[i + 1]);
			if(ret_val == SUCCESS) {
                flagInterval = true;
                if (intervalADV < 100)
                {
                    intervalADV = 100;
                }
                else if (intervalADV > 10000)
                {
                    intervalADV = 10000;
                }
                g_intervalADV = intervalADV / 0.625;
                dione_adv_params.adv_int_max = g_intervalADV;
                dione_adv_params.adv_int_min = g_intervalADV;
				continue;
			}
		}
	}
    if (flagUUID && flagMajor && flagMinor && flagInterval)
    {
        esp_ble_gap_config_adv_data_raw(dione_raw_adv_data, 31);
        xTaskCreate(dione_mqtt_save_ibeacon_task, "saveiBecon", 2048, NULL, 11, NULL);
        return true;
    }
	return false;
}

static void dione_mqtt_save_ibeacon_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    err = nvs_set_u16(my_handle, "adv_interval", g_intervalADV);
    err = nvs_set_blob(my_handle, "dione.minor", gMinor, sizeof(gMinor));
    err = nvs_set_blob(my_handle, "dione.major", gMajor, sizeof(gMajor));
    err = nvs_set_blob(my_handle, "dione.uuid", gUUID, sizeof(gUUID));
    err = nvs_commit(my_handle);
    nvs_close(my_handle);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    vTaskDelete(NULL);
}

bool Dione_isJsonValidAndParse(const char *pJsonDocument, int *pTokenCount)
{
	int tokenCount;

	//IOT_UNUSED(pJsonHandler);

	jsmn_init(&json_parser);

	tokenCount = jsmn_parse(&json_parser, pJsonDocument, strlen(pJsonDocument), token,
							sizeof(token) / sizeof(token[0]));

	if(tokenCount < 0) {
		ESP_LOGE(TAG_DIONE, "Failed to parse JSON: %d\n", tokenCount);
		return false;
	}

	/* Assume the top-level element is an object */
	if(tokenCount < 1 || token[0].type != JSMN_OBJECT) {
		ESP_LOGE(TAG_DIONE, "Top Level is not an object\n");
		return false;
	}

	*pTokenCount = tokenCount;

	return true;
}

#ifdef MQTT_SERVER_SH_HUOWEI
static void resolve_command(char *msg)
{
    int tokenCount = 0;
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    char data[32] = {0};
    if (Dione_isJsonValidAndParse(msg, &tokenCount))
    {
        uint32_t cmdNumber = 0;
        if (Dione_extractCmdNumber(msg, tokenCount, &cmdNumber))
        {
            switch (cmdNumber)
            {
                case 0x1000:
                    xTaskCreate(dione_mqtt_rsp_config_task, "rsp_config", 2048, NULL, 11, NULL);
                    break;
                case 0x1001:
                    if (Dione_extractROMNameServerPort(msg, tokenCount))
                    {
                        ESP_LOGW(TAG_DIONE, "OTA infermation got OK!!!");
                        xTaskCreate(dione_mqtt_rsp_ota_task, "rsp_ota", 2048, NULL, 11, NULL);
                    }
                    else
                    {
                        goto EXITHERE;
                    }
                    break;
                case 0x1002:
                    if (Dione_extractWifi(msg, tokenCount))
                    {
                        ESP_LOGW(TAG_DIONE, "Wifi infermation got OK!!!");
                        goto EXITHERE_OK;
                    }
                    else
                    {
                        goto EXITHERE;
                    }
                    break;
                case 0x1004:
                    if (Dione_extractUUIDMajorMinorInterval(msg, tokenCount))
                    {
                        ESP_LOGW(TAG_DIONE, "iBeacon infermation got OK!!!");
                        goto EXITHERE_OK;
                    }
                    else
                    {
                        goto EXITHERE;
                    }
                    break;
                default:
                    goto EXITHERE;
                    break;
            }
            return;
        }
        goto EXITHERE;
    }
    else if (strncmp(msg, "otastart", 8) == 0)
    {
        char *buf = malloc(64);
        if (gBLEServer == 2)
        {
            esp_ble_gap_stop_advertising();
            esp_blufi_profile_deinit();
            esp_ble_gap_stop_scanning();
        }
        else if (gBLEServer == 1) // This is a server
        {
            esp_ble_gap_stop_advertising();
            esp_blufi_profile_deinit();
        }
        else
        {
            esp_ble_gap_stop_scanning();
        }
        esp_wifi_set_promiscuous(false);
        //blufi_security_deinit();
        if (gHaveUM100 == 1)
        {
            vTaskSuspend(gDioneUARTTH);
        }
        if (gDioneBLE != NULL) vTaskSuspend(gDioneBLE);
        if (gDioneHeartBeat != NULL) vTaskSuspend(gDioneHeartBeat);
        if (gDioneTemp != NULL) vTaskSuspend(gDioneTemp);
        strncpy(buf, "OTA start...please wait a moment.", 64);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, buf, strlen(buf), 0, 0);
        free(buf);
        xTaskCreate(dione_begin_ota_task, "bot", 2048, NULL, 25, NULL);
        return;
    }
    else if (strncmp(msg, "boottime", 8) == 0)
    {
        char *buf = malloc(128);
        sprintf(buf, "Boot times : %d\r\n", g_restart_counter);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, buf, strlen(buf), 0, 0);
        free(buf);
        return;
    }
    else if (strncmp(msg, "timesync", 8) == 0)
    {
        static uint8_t param = 1;
        xTaskCreate(getTime, "getTime", 4096, &param, 1, NULL);
        return;
    }
    else if (strncmp(msg, "time", 4) == 0)
    {
        //char *strftime_buf = malloc(64);
        char strftime_buf[64] = {0};
        time_t now;
        struct tm timeinfo;
        time(&now);
        localtime_r(&now, &timeinfo);
        //memset(strftime_buf, 0, 64);
        strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        //free(strftime_buf);
        return;
    }
    else if (strncmp(msg, "reset", 5) == 0)
    {
        xTaskCreate(dione_mqtt_reset_task, "mqtt_reset", 2048, NULL, 25, NULL);
        return;
    }
    else if (strncmp(msg, "freememory", 10) == 0)
    {
        char *buffers = malloc(64);
        sprintf(buffers, "Free memory: %d bytes\n", esp_get_free_heap_size());
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
        free(buffers);
        return;
    }
    else if (strncmp(msg, "sw", 2) == 0)
    {
        char *buffers = malloc(64);
        sprintf(buffers, "version : %s", DIONE_SW_VERSION);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
        free(buffers);
        return;
    }
    else if (strncmp(msg, "runtime", 7) == 0)
    {
        char *strftime_buf = malloc(128);
        // 1 tick is 10ms.
        uint32_t c = xTaskGetTickCount();
        uint32_t ms = c * portTICK_PERIOD_MS;
        float run_second = ms / 1000.0;
        float run_minute = run_second / 60.0;
        float run_hours = run_minute / 60.0;
        sprintf(strftime_buf, "{\"run_second\":%.2f,\"run_minute\":%.2f,\"run_hours\":%.2f}", run_second, run_minute, run_hours);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        free(strftime_buf);
        return;
    }
    else if (strncmp(msg, "jb on", 5) == 0)
    {
        gJB = true;
        char *buffers = malloc(64);
        strcpy(buffers, "iBeacon judge OPENED!");
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
        free(buffers);
        return;
    }
    else if (strncmp(msg, "jb off", 6) == 0)
    {
        gJB = false;
        char *buffers = malloc(64);
        strcpy(buffers, "iBeacon judge CLOSED!");
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
        free(buffers);
        return;
    }
    else if (strncmp(msg, "daps", 4) == 0)
    {
        if (gHaveUM100 == 0) return;
        #if 1
        //if (strcmp(msg, "daps") != 0)
        {
            eTaskState ets = eTaskGetState(gDioneUARTTH);
            if (ets == eSuspended)
            {
                ESP_LOGE(TAG_DIONE, "Let us resume uart task");
                vTaskResume(gDioneUARTTH);
                vTaskDelay(1000/portTICK_PERIOD_MS);
            }
        }
        #endif
        ESP_LOGE(TAG_DIONE, "Hehe......00");
        uart_flush(UART_NUM_1);
        ESP_LOGE(TAG_DIONE, "Hehe......01");
        //int len = uart_read_bytes(UART_NUM_1, (uint8_t*)data, 32, 20 / portTICK_RATE_MS);
        sprintf(data, "%s\r\n", msg); 
        uart_write_bytes(UART_NUM_1, (const char*)data, strlen(data));
        vTaskDelay(100/portTICK_PERIOD_MS);
        xEventGroupSetBits(dione_event_group, DIONE_EVENT_UWB_START);
        uart_write_bytes(UART_NUM_1, (const char*)data, strlen(data));
#if 1
        if (strncmp(msg, "daps", 32) == 0)
        {
            
            ESP_LOGE(TAG_DIONE, "Let us suspend uart task");
            //vTaskDelay(2000/portTICK_PERIOD_MS);
            //vTaskSuspend(gDioneUARTTH);
            if (gSuspendding == false)
            {
                xTaskCreate(dione_suspend_uart_task, "suspend_task", 2048, NULL, 25, NULL);
            }
        }
        else
        {
            //ESP_LOGE(TAG_DIONE, "Let us resume uart task");
            //vTaskResume(gDioneUARTTH);
            //return;
        }
#endif
        return;
    }
    else if (strncmp(msg, "startUWB", 8) == 0)
    {
        if (gHaveUM100 == 1)
        {
            //vTaskSuspend(gDioneUARTTH);
            uart_flush(UART_NUM_1);
            //vTaskDelay(100/portTICK_PERIOD_MS);
            strncpy((char*)data, "daps 1\r\n", 8);
            uart_write_bytes(UART_NUM_1, (const char*)data, 8);
            vTaskDelay(100/portTICK_PERIOD_MS);
            xEventGroupSetBits(dione_event_group, DIONE_EVENT_UWB_START);
            //vTaskResume(gDioneUARTTH);
            uart_write_bytes(UART_NUM_1, (const char*)data, 8);
        }
        return;
    }
    else if (strncmp(msg, "stopUWB", 7) == 0)
    {
        if (gHaveUM100 == 1)
        {
            uart_flush(UART_NUM_1);
            //vTaskDelay(100/portTICK_PERIOD_MS);
            strncpy((char*)data, "daps\r\n", 6);
            uart_write_bytes(UART_NUM_1, (const char*)data, 6);
            vTaskDelay(100/portTICK_PERIOD_MS);
            uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        }
        return;
    }
    else if (strncmp(msg, "vTaskList", 9) == 0)
    {
        #if 1
        char *buffers = malloc(2048);
        buffers[0] = '\r';
        buffers[1] = '\n';
        vTaskList(buffers + 2);
        int len = strnlen(buffers, 2048);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, len, 0, 0);
        free(buffers);
        return;
        #endif
        goto EXITHERE;
    }
    else
    {
        goto EXITHERE;
    }
    
EXITHERE:
    {
    char *buffers = malloc(64);
    strcpy(buffers, "Command Error!\r\n");
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
    free(buffers);
    return;
    }
EXITHERE_OK:
    {
    char *buffers = malloc(64);
    strcpy(buffers, "Command OK!\r\n");
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)buffers, strlen(buffers), 0, 0);
    free(buffers);
    }

}
#else
static void resolve_command(char *msg)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    char data[32] = {0};
    if (strncmp(msg, "otastart", 8) == 0)
    {
        char *buf = malloc(64);
        if (gBLEServer == 2)
        {
            esp_ble_gap_stop_advertising();
            esp_blufi_profile_deinit();
            esp_ble_gap_stop_scanning();
        }
        else if (gBLEServer == 1) // This is a server
        {
            esp_ble_gap_stop_advertising();
            esp_blufi_profile_deinit();
        }
        else
        {
            esp_ble_gap_stop_scanning();
        }
        esp_wifi_set_promiscuous(false);
        //blufi_security_deinit();
        if (gHaveUM100 == 1)
        {
            vTaskSuspend(gDioneUARTTH);
        }
        if (gDioneBLE != NULL) vTaskSuspend(gDioneBLE);
        if (gDioneHeartBeat != NULL) vTaskSuspend(gDioneHeartBeat);
        if (gDioneTemp != NULL) vTaskSuspend(gDioneTemp);
        strncpy(buf, "OTA start...please wait a moment.", 64);
        mqtt_publish(g_mqtt_client, "Dione", buf, strlen(buf), 0, 0);
        free(buf);
        xTaskCreate(dione_begin_ota_task, "bot", 2048, NULL, 25, NULL);
        
    }
    else if (strncmp(msg, "startUWB", 8) == 0)
    {
        if (gHaveUM100 == 1)
        {
            //vTaskSuspend(gDioneUARTTH);
            uart_flush(UART_NUM_1);
            //vTaskDelay(100/portTICK_PERIOD_MS);
            strncpy((char*)data, "daps 0\r\n", 8);
            uart_write_bytes(UART_NUM_1, (const char*)data, 8);
            vTaskDelay(100/portTICK_PERIOD_MS);
            xEventGroupSetBits(dione_event_group, DIONE_EVENT_UWB_START);
            //vTaskResume(gDioneUARTTH);
            uart_write_bytes(UART_NUM_1, (const char*)data, 8);
        }
    }
    else if (strncmp(msg, "stopUWB", 7) == 0)
    {
        if (gHaveUM100 == 1)
        {
            uart_flush(UART_NUM_1);
            //vTaskDelay(100/portTICK_PERIOD_MS);
            strncpy((char*)data, "daps\r\n", 6);
            uart_write_bytes(UART_NUM_1, (const char*)data, 6);
            vTaskDelay(100/portTICK_PERIOD_MS);
            uart_write_bytes(UART_NUM_1, (const char*)data, 6);
        }
    }
    else if (strncmp(msg, "ibeacon", 7) == 0)
    {
        dione_nvs_save_bleServer(1);
        xTaskCreate(dione_mqtt_reset_task, "mqtt_reset", 2048, NULL, 25, NULL);
    }
    else if (strncmp(msg, "blestation", 10) == 0)
    {
        dione_nvs_save_bleServer(0);
        xTaskCreate(dione_mqtt_reset_task, "mqtt_reset", 2048, NULL, 25, NULL);
    }
    else if (strncmp(msg, "boottime", 8) == 0)
    {
        char *buf = malloc(128);
        sprintf(buf, "Boot times : %d\r\n", g_restart_counter);
        mqtt_publish(g_mqtt_client, "Dione", buf, strlen(buf), 0, 0);
        free(buf);
    }
    else if (strncmp(msg, "timesync", 8) == 0)
    {
        static uint8_t param = 1;
        xTaskCreate(getTime, "getTime", 4096, &param, 1, NULL);
    }
    else if (strncmp(msg, "time", 4) == 0)
    {
        char strftime_buf[64];
        time_t now;
        struct tm timeinfo;
        time(&now);
        localtime_r(&now, &timeinfo);
        strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
        mqtt_publish(g_mqtt_client, "Dione", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
    }
    else if (strncmp(msg, "reset", 5) == 0)
    {
        xTaskCreate(dione_mqtt_reset_task, "mqtt_reset", 2048, NULL, 25, NULL);
    }
    else if (strncmp(msg, "vTaskList", 9) == 0)
    {
        #if 0
        char *buffers = malloc(2048);
        buffers[0] = '\r';
        buffers[1] = '\n';
        vTaskList(buffers + 2);
        int len = strnlen(buffers, 2048);
        mqtt_publish(g_mqtt_client, "Dione", (char*)buffers, len, 0, 0);
        free(buffers);
        #endif
    }
    else if (strncmp(msg, "freememory", 10) == 0)
    {
        char buffers[64] = {0};
        sprintf(buffers, "Free memory: %d bytes\n", esp_get_free_heap_size());
        mqtt_publish(g_mqtt_client, "Dione", (char*)buffers, strlen(buffers), 0, 0);
    }
    else if (strncmp(msg, "sw", 2) == 0)
    {
        char buffers[32] = {0};
        sprintf(buffers, "version : %s", DIONE_SW_VERSION);
        mqtt_publish(g_mqtt_client, "Dione", (char*)buffers, strlen(buffers), 0, 0);
    }
    else
    {
        if (gHaveUM100 == 0) return;
        #if 1
        //if (strcmp(msg, "daps") != 0)
        {
            eTaskState ets = eTaskGetState(gDioneUARTTH);
            if (ets == eSuspended)
            {
                ESP_LOGE(TAG_DIONE, "Let us resume uart task");
                vTaskResume(gDioneUARTTH);
                vTaskDelay(1000/portTICK_PERIOD_MS);
            }
        }
        #endif
        ESP_LOGE(TAG_DIONE, "Hehe......00");
        uart_flush(UART_NUM_1);
        ESP_LOGE(TAG_DIONE, "Hehe......01");
        //int len = uart_read_bytes(UART_NUM_1, (uint8_t*)data, 32, 20 / portTICK_RATE_MS);
        sprintf(data, "%s\r\n", msg); 
        uart_write_bytes(UART_NUM_1, (const char*)data, strlen(data));
        vTaskDelay(100/portTICK_PERIOD_MS);
        xEventGroupSetBits(dione_event_group, DIONE_EVENT_UWB_START);
        uart_write_bytes(UART_NUM_1, (const char*)data, strlen(data));
#if 1
        if (strncmp(msg, "daps", 32) == 0)
        {
            
            ESP_LOGE(TAG_DIONE, "Let us suspend uart task");
            //vTaskDelay(2000/portTICK_PERIOD_MS);
            //vTaskSuspend(gDioneUARTTH);
            if (gSuspendding == false)
            {
                xTaskCreate(dione_suspend_uart_task, "suspend_task", 2048, NULL, 25, NULL);
            }
        }
        else
        {
            //ESP_LOGE(TAG_DIONE, "Let us resume uart task");
            //vTaskResume(gDioneUARTTH);
            //return;
        }
#endif
    }
}
#endif

static void data_cb(void *self, void *params)
{
    //mqtt_client *client = (mqtt_client *)self;
    mqtt_event_data_t *event_data = (mqtt_event_data_t *)params;
    if (event_data->topic_length > 32)
    {
        ESP_LOGE(TAG_DIONE, "topic length too big : %d", event_data->topic_length);
        return;
    }
    char *topic = malloc(event_data->topic_length + 1);
    if (event_data->data_offset == 0) {
        memcpy(topic, event_data->topic, event_data->topic_length);
        topic[event_data->topic_length] = 0;
        mqtt_info("[APP] Publish topic: %s\n", topic);
    }

    // char *data = malloc(event_data->data_length + 1);
    // memcpy(data, event_data->data, event_data->data_length);
    // data[event_data->data_length] = 0;
    mqtt_info("[APP] Publish data[%d/%d bytes]\ntype[%d]\n",
         event_data->data_length + event_data->data_offset,
         event_data->data_total_length,
         event_data->type);
         // data);

    // free(data);
    if (event_data->data_length != 0 && strcmp(topic, MQTT_SUB_TOPIC)==0)
    {
        char *msg1 = malloc(event_data->data_length + 1);
        memcpy(msg1, event_data->data, event_data->data_length);
        msg1[event_data->data_length] = 0;
        mqtt_info("[APP] msg: %s", msg1);
        resolve_command(msg1);
        free(msg1);
    }
    free(topic);
}

#ifdef SECOND_MQTT_SERVER
static void data_cb2(void *self, void *params)
{
    //mqtt_client *client = (mqtt_client *)self;
    dione_mqtt_event_data_t *event_data = (dione_mqtt_event_data_t *)params;
    if (event_data->topic_length > 32)
    {
        ESP_LOGE(TAG_DIONE, "topic length too big : %d", event_data->topic_length);
        return;
    }
    char *topic = malloc(event_data->topic_length + 1);
    if (event_data->data_offset == 0) {
        memcpy(topic, event_data->topic, event_data->topic_length);
        topic[event_data->topic_length] = 0;
        mqtt_info("[APP] Publish topic: %s\n", topic);
    }

    // char *data = malloc(event_data->data_length + 1);
    // memcpy(data, event_data->data, event_data->data_length);
    // data[event_data->data_length] = 0;
    mqtt_info("[APP] Publish data[%d/%d bytes]\ntype[%d]\n",
         event_data->data_length + event_data->data_offset,
         event_data->data_total_length,
         event_data->type);
         // data);

    // free(data);
    if (event_data->data_length != 0 && strcmp(topic, MQTT_SUB_TOPIC)==0)
    {
        char *msg1 = malloc(event_data->data_length + 1);
        memcpy(msg1, event_data->data, event_data->data_length);
        msg1[event_data->data_length] = 0;
        mqtt_info("[APP] msg: %s", msg1);
        resolve_command(msg1);
        free(msg1);
    }
    free(topic);
}
#endif

#ifdef MQTT_SERVER_DIONE
static mqtt_settings settings = {
    //.host = "test.mosquitto.org",
    .host = "www.huoweiyi.com",
#if defined(CONFIG_MQTT_SECURITY_ON)         
    .port = 8883, // encrypted
#else
    .port = 1883, // unencrypted
#endif    
    .client_id = "mqtt_client_id",
    .username = "",
    .password = "",
    .clean_session = 0,
    .keepalive = 120,
    .lwt_topic = "/lwt",
    //.lwt_topic = "",
    .lwt_msg = "offline",
    //.lwt_msg = "",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb,
    .disconnected_cb = disconnected_cb,
    .reconnect_cb = reconnect_cb,
    .subscribe_cb = subscribe_cb,
    .publish_cb = publish_cb,
    .data_cb = data_cb
};
#elif defined(MQTT_SERVER_ONENET)
static mqtt_settings settings = {
    .host = "183.230.40.39",
#if defined(CONFIG_MQTT_SECURITY_ON)         
    .port = 8883, // encrypted
#else
    .port = 6002, // unencrypted
#endif    
    .client_id = "7098786",
    .username = "88719",
    .password = "W6fqC4dt9Q4z90O1lxy",
    .clean_session = 0,
    .keepalive = 120,
    .lwt_topic = "",
    .lwt_msg = "",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb,
    .disconnected_cb = disconnected_cb,
    .reconnect_cb = reconnect_cb,
    .subscribe_cb = subscribe_cb,
    .publish_cb = publish_cb,
    .data_cb = data_cb
};
#elif defined(MQTT_SERVER_MICRODUINO)
static mqtt_settings settings = {
    //.host = "test.mosquitto.org",
    .host = "mcotton.microduino.cn",
#if defined(CONFIG_MQTT_SECURITY_ON)         
    .port = 8883, // encrypted
#else
    .port = 1883, // unencrypted
#endif    
    .client_id = "5936377a4eb6400001eb131a",
    .username =  "5936377a4eb6400001eb131a",
    .password = "JfmDM8rhcYU2",
    .clean_session = 0,
    .keepalive = 120,
    .lwt_topic = "/lwt",
    //.lwt_topic = "",
    .lwt_msg = "offline",
    //.lwt_msg = "",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb,
    .disconnected_cb = disconnected_cb,
    .reconnect_cb = reconnect_cb,
    .subscribe_cb = subscribe_cb,
    .publish_cb = publish_cb,
    .data_cb = data_cb
};
#elif defined(MQTT_SERVER_SH_HUOWEI)
static mqtt_settings settings = {
    //.host = "test.mosquitto.org",
    .host = "www.huoweiyi.com",
    //.host = "61.152.175.119",
#if defined(CONFIG_MQTT_SECURITY_ON)         
    .port = 8883, // encrypted
#else
    .port = 1883, // unencrypted
#endif    
    .client_id = "", // mac address
    .username = "",
    .password = "",
    .clean_session = 0,
    .keepalive = 120,
    .lwt_topic = "/lwt",
    //.lwt_topic = "",
    .lwt_msg = "offline",
    //.lwt_msg = "",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb,
    .disconnected_cb = disconnected_cb,
    .reconnect_cb = reconnect_cb,
    .subscribe_cb = subscribe_cb,
    .publish_cb = publish_cb,
    .data_cb = data_cb
};
#endif

#ifdef SECOND_MQTT_SERVER
static mqtt_settings settings2 = {
    //.host = "test.mosquitto.org",
    .host = "mcotton.microduino.cn",
#if defined(CONFIG_MQTT_SECURITY_ON)         
    .port = 8883, // encrypted
#else
    .port = 1883, // unencrypted
#endif    
    .client_id = "5936377a4eb6400001eb131a",
    .username =  "5936377a4eb6400001eb131a",
    .password = "JfmDM8rhcYU2",
    .clean_session = 0,
    .keepalive = 120,
    .lwt_topic = "/lwt",
    //.lwt_topic = "",
    .lwt_msg = "offline",
    //.lwt_msg = "",
    .lwt_qos = 0,
    .lwt_retain = 0,
    .connected_cb = connected_cb2,
    .disconnected_cb = disconnected_cb2,
    .reconnect_cb = reconnect_cb2,
    .subscribe_cb = subscribe_cb2,
    .publish_cb = publish_cb2,
    .data_cb = data_cb2
};
#endif

static void dione_wifi_event_str(system_event_id_t evt_id)
{
    char buf[64] = {0};
    switch (evt_id)
    {
    case SYSTEM_EVENT_WIFI_READY:           /**< ESP32 WiFi ready */
        strcpy(buf, "SYSTEM_EVENT_WIFI_READY");
        break;
    case SYSTEM_EVENT_SCAN_DONE:                /**< ESP32 finish scanning AP */
        strcpy(buf, "SYSTEM_EVENT_SCAN_DONE");
        break;
    case SYSTEM_EVENT_STA_START:                /**< ESP32 station start */
        strcpy(buf, "SYSTEM_EVENT_STA_START");
        break;
    case SYSTEM_EVENT_STA_STOP:                 /**< ESP32 station stop */
        strcpy(buf, "SYSTEM_EVENT_STA_STOP");
        break;
    case SYSTEM_EVENT_STA_CONNECTED:            /**< ESP32 station connected to AP */
        strcpy(buf, "SYSTEM_EVENT_STA_CONNECTED");
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:         /**< ESP32 station disconnected from AP */
        strcpy(buf, "SYSTEM_EVENT_STA_DISCONNECTED");
        break;
    case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:      /**< the auth mode of AP connected by ESP32 station changed */
        strcpy(buf, "SYSTEM_EVENT_STA_AUTHMODE_CHANGE");
        break;
    case SYSTEM_EVENT_STA_GOT_IP:               /**< ESP32 station got IP from connected AP */
        strcpy(buf, "SYSTEM_EVENT_STA_GOT_IP");
        break;
    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:       /**< ESP32 station wps succeeds in enrollee mode */
        strcpy(buf, "SYSTEM_EVENT_STA_WPS_ER_SUCCESS");
        break;
    case SYSTEM_EVENT_STA_WPS_ER_FAILED:        /**< ESP32 station wps fails in enrollee mode */
        strcpy(buf, "SYSTEM_EVENT_STA_WPS_ER_FAILED");
        break;
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:       /**< ESP32 station wps timeout in enrollee mode */
        strcpy(buf, "SYSTEM_EVENT_STA_WPS_ER_TIMEOUT");
        break;
    case SYSTEM_EVENT_STA_WPS_ER_PIN:           /**< ESP32 station wps pin code in enrollee mode */
        strcpy(buf, "SYSTEM_EVENT_STA_WPS_ER_PIN");
        break;
    case SYSTEM_EVENT_AP_START:                 /**< ESP32 soft-AP start */
        strcpy(buf, "SYSTEM_EVENT_AP_START");
        break;
    case SYSTEM_EVENT_AP_STOP:                  /**< ESP32 soft-AP stop */
        strcpy(buf, "SYSTEM_EVENT_AP_STOP");
        break;
    case SYSTEM_EVENT_AP_STACONNECTED:          /**< a station connected to ESP32 soft-AP */
        strcpy(buf, "SYSTEM_EVENT_AP_STACONNECTED");
        break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:       /**< a station disconnected from ESP32 soft-AP */
        strcpy(buf, "SYSTEM_EVENT_AP_STADISCONNECTED");
        break;
    case SYSTEM_EVENT_AP_PROBEREQRECVED:        /**< Receive probe request packet in soft-AP interface */
        strcpy(buf, "SYSTEM_EVENT_AP_PROBEREQRECVED");
        break;
    case SYSTEM_EVENT_AP_STA_GOT_IP6:           /**< ESP32 station or ap interface v6IP addr is preferred */
        strcpy(buf, "SYSTEM_EVENT_AP_STA_GOT_IP6");
        break;
    case SYSTEM_EVENT_ETH_START:                /**< ESP32 ethernet start */
        strcpy(buf, "SYSTEM_EVENT_ETH_START");
        break;
    case SYSTEM_EVENT_ETH_STOP:                 /**< ESP32 ethernet stop */
        strcpy(buf, "SYSTEM_EVENT_ETH_STOP");
        break;
    case SYSTEM_EVENT_ETH_CONNECTED:            /**< ESP32 ethernet phy link up */
        strcpy(buf, "SYSTEM_EVENT_ETH_CONNECTED");
        break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:         /**< ESP32 ethernet phy link down */
        strcpy(buf, "SYSTEM_EVENT_ETH_DISCONNECTED");
        break;
    case SYSTEM_EVENT_ETH_GOT_IP:               /**< ESP32 ethernet got IP from connected AP */
        strcpy(buf, "SYSTEM_EVENT_ETH_GOT_IP");
        break;
    case SYSTEM_EVENT_MAX:
        strcpy(buf, "SYSTEM_EVENT_MAX");
        break;
    default:
        strcpy(buf, "OH, what event?!");
        break;
    }
    ESP_LOGW(TAG_DIONE, "event_id = %s", buf);
}

static void dione_wifi_stop_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    //esp_wifi_connect();
    esp_wifi_stop();
    ESP_LOGW(TAG_DIONE, "Leave %s", __func__);
    vTaskDelete(NULL);
}

static void dione_wifi_init_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_FLASH) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(gWifiMode) );
    if (WIFI_MODE_STA == gWifiMode)
    {
        strncpy((char*)sta_config.sta.ssid, (char*)gSTA_ssid, sizeof(gSTA_ssid));
        strncpy((char*)sta_config.sta.password, (char*)gSTA_pass, sizeof(gSTA_pass));
        esp_wifi_set_config(ESP_IF_WIFI_STA, &sta_config);
    }
    else if (WIFI_MODE_AP == gWifiMode)
    {
        strncpy((char*)ap_config.ap.ssid, (char*)gAP_ssid, sizeof(gAP_ssid));
        strncpy((char*)ap_config.ap.password, (char*)gAP_pass, sizeof(gAP_pass));
        ap_config.ap.max_connection = gAP_max_connection;
        ap_config.ap.authmode = gAP_auth_mode;
        if (gAP_channel != -1)
        {
            ap_config.ap.channel = gAP_channel;
        }
        esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config);
    }
    else if (WIFI_MODE_APSTA == gWifiMode)
    {
        strncpy((char*)sta_config.sta.ssid, (char*)gSTA_ssid, sizeof(gSTA_ssid));
        strncpy((char*)sta_config.sta.password, (char*)gSTA_pass, sizeof(gSTA_pass));
        esp_wifi_set_config(ESP_IF_WIFI_STA, &sta_config);
        strncpy((char*)ap_config.ap.ssid, (char*)gAP_ssid, sizeof(gAP_ssid));
        strncpy((char*)ap_config.ap.password, (char*)gAP_pass, sizeof(gAP_pass));
        ap_config.ap.max_connection = gAP_max_connection;
        ap_config.ap.authmode = gAP_auth_mode;
        if (gAP_channel != -1)
        {
            ap_config.ap.channel = gAP_channel;
        }
        esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config);
    }
    
    ESP_ERROR_CHECK(esp_wifi_set_promiscuous_rx_cb(dione_wifi_promiscuous));
    //bool promiscuous = true;
    //ESP_ERROR_CHECK(esp_wifi_set_promiscuous(true));
    //ESP_ERROR_CHECK( esp_wifi_start() );
    esp_wifi_start();
    vTaskDelete(NULL);
}

static void dione_wifi_deinit_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    esp_wifi_deinit();
    xTaskCreate(dione_wifi_init_task, "wifi_init", 4096, NULL, 3, NULL);
    ESP_LOGW(TAG_DIONE, "Leave %s", __func__);
    vTaskDelete(NULL);
}

static void dione_wifi_connect_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    vTaskDelay(3000 / portTICK_PERIOD_MS);
    esp_wifi_connect();
    ESP_LOGW(TAG_DIONE, "Leave %s", __func__);
    vTaskDelete(NULL);
}

static esp_err_t dione_wifi_event_handler(void *ctx, system_event_t *event)
{
    ESP_LOGW(TAG_DIONE, "Enter %s(), event_id=%d", __func__, event->event_id);
    dione_wifi_event_str(event->event_id);
    wifi_mode_t mode;

    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        //dione_uninitialise_ethernet();
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_STOP:
        //xTaskCreate(dione_wifi_deinit_task, "wifi_deinit", 4096, NULL, 2, NULL);
        break;
    case SYSTEM_EVENT_STA_GOT_IP: {
        if (!gIfGotIP)
        {
            gIfGotIP = true;
            dione_uninitialise_ethernet();
            if (g_mqtt_client == NULL)
            {
                g_mqtt_client = mqtt_start(&settings);
#ifdef SECOND_MQTT_SERVER
                g_mqtt_client2 = dione_mqtt_start(&settings2);
#endif
                xEventGroupSetBits(dione_event_group, DIONE_EVENT_STA_GOT_IP);
            }
            if (gBLEServer == 1 || gBLEServer == 2)
            {
                esp_blufi_extra_info_t info;
                esp_wifi_get_mode(&mode);
                memset(&info, 0, sizeof(esp_blufi_extra_info_t));
                memcpy(info.sta_bssid, gl_sta_bssid, 6);
                info.sta_bssid_set = true;
                info.sta_ssid = gl_sta_ssid;
                info.sta_ssid_len = gl_sta_ssid_len;
                esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_SUCCESS, 0, &info);
            }
        }
        break;
    }
    case SYSTEM_EVENT_STA_CONNECTED:
        gl_sta_connected = true;
        memcpy(gl_sta_bssid, event->event_info.connected.bssid, 6);
        memcpy(gl_sta_ssid, event->event_info.connected.ssid, event->event_info.connected.ssid_len);
        gl_sta_ssid_len = event->event_info.connected.ssid_len;
        break; 
    case SYSTEM_EVENT_STA_DISCONNECTED:
        gSTA_DISCONNECTED_counts++;
        if (gSTA_DISCONNECTED_counts >= 10)
        {
            esp_restart();
        }
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        gl_sta_connected = false;
        memset(gl_sta_ssid, 0, 32);
        memset(gl_sta_bssid, 0, 6);
        gl_sta_ssid_len = 0;
        #if 0
        if (gSTA_DISCONNECTED_counts >= 10 || event->event_info.disconnected.reason == WIFI_REASON_NOT_AUTHED||
            WIFI_REASON_AUTH_EXPIRE == event->event_info.disconnected.reason ||
            WIFI_REASON_NO_AP_FOUND == event->event_info.disconnected.reason)
        {
            //esp_restart();
            gSTA_DISCONNECTED_counts = 0;
            xTaskCreate(dione_wifi_stop_task, "wifi_stop", 4096, NULL, 2, NULL);
            
        }
        #endif
        xTaskCreate(dione_wifi_connect_task, "wifi_connect", 4096, NULL, 24, NULL);
        //esp_wifi_connect();
        xEventGroupClearBits(dione_event_group, DIONE_EVENT_STA_GOT_IP);
        break;
    case SYSTEM_EVENT_AP_START:
        if (gBLEServer == 1 || gBLEServer == 2)
        {
            esp_wifi_get_mode(&mode);
            /* TODO: get config or information of softap, then set to report extra_info */
            if (gl_sta_connected) {  
                esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_SUCCESS, 0, NULL);
            } else {
                esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_FAIL, 0, NULL);
            }
        }
        break;

    
    case SYSTEM_EVENT_ETH_START:
        break;
    case SYSTEM_EVENT_ETH_STOP:
        break;
    case SYSTEM_EVENT_ETH_CONNECTED:
        break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
        gIfGotIP = false;
        break;
    case SYSTEM_EVENT_ETH_GOT_IP:
        if (!gIfGotIP)
        {
            gIfGotIP = true;
            esp_wifi_stop();
            esp_wifi_deinit();
            if (g_mqtt_client == NULL)
            {
                g_mqtt_client = mqtt_start(&settings);
#ifdef SECOND_MQTT_SERVER
                g_mqtt_client2 = dione_mqtt_start(&settings2);
#endif
                xEventGroupSetBits(dione_event_group, DIONE_EVENT_STA_GOT_IP);
            }
        }
#if 0
        if (gBLEServer == 1)
        {
            esp_blufi_extra_info_t info;
            esp_wifi_get_mode(&mode);
            memset(&info, 0, sizeof(esp_blufi_extra_info_t));
            memcpy(info.sta_bssid, gl_sta_bssid, 6);
            info.sta_bssid_set = true;
            info.sta_ssid = gl_sta_ssid;
            info.sta_ssid_len = gl_sta_ssid_len;
            esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_SUCCESS, 0, &info);
        }
#endif
        break;
    
    default:
        break;
    }
    return ESP_OK;
}

static void dione_wifi_promiscuous(void *buf, wifi_promiscuous_pkt_type_t type)
{
    #if 1
    static uint32_t counts = 0;
    static uint32_t level = 1;
    //ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    switch (type)
    {
        case WIFI_PKT_MGMT:
            //ESP_LOGI(TAG_DIONE, "WIFI_PKT_MGMT");
            break;
        case WIFI_PKT_DATA:
            //ESP_LOGI(TAG_DIONE, "WIFI_PKT_DATA");
            if (++counts % 3 == 0)
            {
                gpio_set_level(LED_WIFI, level);
                level = !level;
            }
            break;
        case WIFI_PKT_MISC:
            //ESP_LOGI(TAG_DIONE, "WIFI_PKT_MISC");
            break;
    }
    #else
    static uint32_t counts = 0;
    static uint32_t level = 1;
    if (++counts % 40 == 0)
    {
        gpio_set_level(LED_WIFI, level);
        level = !level;
    }
    #endif
}

static void dione_initialise_wifi(void)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    ESP_ERROR_CHECK( esp_wifi_set_mode(gWifiMode) );
    if (WIFI_MODE_STA == gWifiMode)
    {
        strncpy((char*)sta_config.sta.ssid, (char*)gSTA_ssid, sizeof(gSTA_ssid));
        strncpy((char*)sta_config.sta.password, (char*)gSTA_pass, sizeof(gSTA_pass));
        esp_wifi_set_config(ESP_IF_WIFI_STA, &sta_config);
    }
    else if (WIFI_MODE_AP == gWifiMode)
    {
        strncpy((char*)ap_config.ap.ssid, (char*)gAP_ssid, sizeof(gAP_ssid));
        strncpy((char*)ap_config.ap.password, (char*)gAP_pass, sizeof(gAP_pass));
        ap_config.ap.max_connection = gAP_max_connection;
        ap_config.ap.authmode = gAP_auth_mode;
        if (gAP_channel != -1)
        {
            ap_config.ap.channel = gAP_channel;
        }
        esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config);
    }
    else if (WIFI_MODE_APSTA == gWifiMode)
    {
        strncpy((char*)sta_config.sta.ssid, (char*)gSTA_ssid, sizeof(gSTA_ssid));
        strncpy((char*)sta_config.sta.password, (char*)gSTA_pass, sizeof(gSTA_pass));
        esp_wifi_set_config(ESP_IF_WIFI_STA, &sta_config);
        strncpy((char*)ap_config.ap.ssid, (char*)gAP_ssid, sizeof(gAP_ssid));
        strncpy((char*)ap_config.ap.password, (char*)gAP_pass, sizeof(gAP_pass));
        ap_config.ap.max_connection = gAP_max_connection;
        ap_config.ap.authmode = gAP_auth_mode;
        if (gAP_channel != -1)
        {
            ap_config.ap.channel = gAP_channel;
        }
        esp_wifi_set_config(ESP_IF_WIFI_AP, &ap_config);
    }
    #if 0
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = WIFI_SSID,
            .password = WIFI_PASS
        },
    };
    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    #endif
    
    ESP_ERROR_CHECK(esp_wifi_set_promiscuous_rx_cb(dione_wifi_promiscuous));
    //bool promiscuous = true;
    //ESP_ERROR_CHECK(esp_wifi_set_promiscuous(true));
    ESP_ERROR_CHECK( esp_wifi_start() );
#if 0
    if (WIFI_MODE_STA == gWifiMode)
    {
        ESP_LOGE(TAG_DIONE, "esp_wifi_set_ps().");
        esp_wifi_set_ps(WIFI_PS_MODEM);
    }
#endif
}

static esp_blufi_callbacks_t dione_callbacks = {
    .event_cb = dione_blufi_event_callback,
    .negotiate_data_handler = blufi_dh_negotiate_data_handler,
    .encrypt_func = blufi_aes_encrypt,
    .decrypt_func = blufi_aes_decrypt,
    .checksum_func = blufi_crc_checksum,
};

static uint8_t server_if;
static uint16_t conn_id;
static void dione_blufi_event_callback(esp_blufi_cb_event_t event, esp_blufi_cb_param_t *param)
{
#ifdef BLE_LED_IN_CALLBACK
    static uint32_t level = 1;
#endif
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    /* actually, should post to blufi_task handle the procedure,
     * now, as a example, we do it more simply */
    switch (event) {
    case ESP_BLUFI_EVENT_INIT_FINISH:
        BLUFI_INFO("BLUFI init finish\n");

        //esp_ble_gap_set_device_name(DIONE_DEVICE_NAME);
        //esp_ble_gap_config_adv_data(&dione_adv_data);
        break;
    case ESP_BLUFI_EVENT_DEINIT_FINISH:
        BLUFI_INFO("BLUFI deinit finish\n");
        break;
    case ESP_BLUFI_EVENT_BLE_CONNECT:
        BLUFI_INFO("BLUFI ble connect\n");
        server_if=param->connect.server_if;
        conn_id=param->connect.conn_id;
        esp_ble_gap_stop_advertising();
        blufi_security_deinit();
        blufi_security_init();
        break;
    case ESP_BLUFI_EVENT_BLE_DISCONNECT:
        BLUFI_INFO("BLUFI ble disconnect\n");
        //esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_BLUFI_EVENT_SET_WIFI_OPMODE:
        BLUFI_INFO("BLUFI Set WIFI opmode %d\n", param->wifi_mode.op_mode);
        ESP_ERROR_CHECK( esp_wifi_set_mode(param->wifi_mode.op_mode) );
        gWifiMode = param->wifi_mode.op_mode;
        dione_nvs_save_wifi_mode();
        break;
    case ESP_BLUFI_EVENT_REQ_CONNECT_TO_AP:
        BLUFI_INFO("BLUFI requset wifi connect to AP\n");
        esp_wifi_connect();
        break;
    case ESP_BLUFI_EVENT_REQ_DISCONNECT_FROM_AP:
        BLUFI_INFO("BLUFI requset wifi disconnect from AP\n");
        esp_wifi_disconnect();
        break;
    case ESP_BLUFI_EVENT_GET_WIFI_STATUS: {
        wifi_mode_t mode;
        esp_blufi_extra_info_t info;

        esp_wifi_get_mode(&mode);

        if (gl_sta_connected ) {  
            memset(&info, 0, sizeof(esp_blufi_extra_info_t));
            memcpy(info.sta_bssid, gl_sta_bssid, 6);
            info.sta_bssid_set = true;
            info.sta_ssid = gl_sta_ssid;
            info.sta_ssid_len = gl_sta_ssid_len;
            esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_SUCCESS, 0, &info);
        } else {
            esp_blufi_send_wifi_conn_report(mode, ESP_BLUFI_STA_CONN_FAIL, 0, NULL);
        }
        BLUFI_INFO("BLUFI get wifi status from AP\n");

        break;
    }
    case ESP_BLUFI_EVENT_RECV_SLAVE_DISCONNECT_BLE:
        BLUFI_INFO("blufi close a gatt connection");
        esp_blufi_close(server_if,conn_id);
        break;
    case ESP_BLUFI_EVENT_DEAUTHENTICATE_STA:
        /* TODO */
        break;
	case ESP_BLUFI_EVENT_RECV_STA_BSSID:
        memcpy(sta_config.sta.bssid, param->sta_bssid.bssid, 6);
        sta_config.sta.bssid_set = 1;
        esp_wifi_set_config(WIFI_IF_STA, &sta_config);
        BLUFI_INFO("Recv STA BSSID %s\n", sta_config.sta.ssid);
        memcpy(gSTA_bssid, param->sta_bssid.bssid, 6);
        break;
	case ESP_BLUFI_EVENT_RECV_STA_SSID:
        strncpy((char *)sta_config.sta.ssid, (char *)param->sta_ssid.ssid, param->sta_ssid.ssid_len);
        sta_config.sta.ssid[param->sta_ssid.ssid_len] = '\0';
        esp_wifi_set_config(WIFI_IF_STA, &sta_config);
        BLUFI_INFO("Recv STA SSID %s\n", sta_config.sta.ssid);
        memcpy(gSTA_ssid, sta_config.sta.ssid, sizeof(sta_config.sta.ssid));
        dione_nvs_save_sta_ssid();
        break;
	case ESP_BLUFI_EVENT_RECV_STA_PASSWD:
        strncpy((char *)sta_config.sta.password, (char *)param->sta_passwd.passwd, param->sta_passwd.passwd_len);
        sta_config.sta.password[param->sta_passwd.passwd_len] = '\0';
        esp_wifi_set_config(WIFI_IF_STA, &sta_config);
        BLUFI_INFO("Recv STA PASSWORD %s\n", sta_config.sta.password);
        memcpy(gSTA_pass, sta_config.sta.password, sizeof(sta_config.sta.password));
        dione_nvs_save_sta_pass();
        break;
	case ESP_BLUFI_EVENT_RECV_SOFTAP_SSID:
        strncpy((char *)ap_config.ap.ssid, (char *)param->softap_ssid.ssid, param->softap_ssid.ssid_len);
        ap_config.ap.ssid_len = param->softap_ssid.ssid_len;
        esp_wifi_set_config(WIFI_IF_AP, &ap_config);
        BLUFI_INFO("Recv SOFTAP SSID %s, ssid len %d\n", ap_config.ap.ssid, ap_config.ap.ssid_len);
        memcpy(gAP_ssid, ap_config.ap.ssid, sizeof(ap_config.ap.ssid));
        dione_nvs_save_ap_ssid();
        break;
	case ESP_BLUFI_EVENT_RECV_SOFTAP_PASSWD:
        strncpy((char *)ap_config.ap.password, (char *)param->softap_passwd.passwd, param->softap_passwd.passwd_len);
        esp_wifi_set_config(WIFI_IF_AP, &ap_config);
        BLUFI_INFO("Recv SOFTAP PASSWORD %s\n", ap_config.ap.password);
        memcpy(gAP_pass, ap_config.ap.password, sizeof(ap_config.ap.password));
        dione_nvs_save_ap_pass();
        break;
	case ESP_BLUFI_EVENT_RECV_SOFTAP_MAX_CONN_NUM:
        if (param->softap_max_conn_num.max_conn_num > 4) {
            return;
        }
        ap_config.ap.max_connection = param->softap_max_conn_num.max_conn_num;
        esp_wifi_set_config(WIFI_IF_AP, &ap_config);
        BLUFI_INFO("Recv SOFTAP MAX CONN NUM %d\n", ap_config.ap.max_connection);
        gAP_max_connection = param->softap_max_conn_num.max_conn_num;
        dione_nvs_save_ap_max_conn_num();
        break;
	case ESP_BLUFI_EVENT_RECV_SOFTAP_AUTH_MODE:
        if (param->softap_auth_mode.auth_mode >= WIFI_AUTH_MAX) {
            return;
        }
        ap_config.ap.authmode = param->softap_auth_mode.auth_mode;
        esp_wifi_set_config(WIFI_IF_AP, &ap_config);
        BLUFI_INFO("Recv SOFTAP AUTH MODE %d\n", ap_config.ap.authmode);
        gAP_auth_mode = param->softap_auth_mode.auth_mode;
        dione_nvs_save_ap_auth_mode();
        break;
	case ESP_BLUFI_EVENT_RECV_SOFTAP_CHANNEL:
        if (param->softap_channel.channel > 13) {
            return;
        }
        ap_config.ap.channel = param->softap_channel.channel;
        esp_wifi_set_config(WIFI_IF_AP, &ap_config);
        BLUFI_INFO("Recv SOFTAP CHANNEL %d\n", ap_config.ap.channel);
        gAP_channel = param->softap_channel.channel;
        dione_nvs_save_ap_channel();
        break;
	case ESP_BLUFI_EVENT_RECV_USERNAME:
        /* Not handle currently */
        break;
	case ESP_BLUFI_EVENT_RECV_CA_CERT:
        /* Not handle currently */
        break;
	case ESP_BLUFI_EVENT_RECV_CLIENT_CERT:
        /* Not handle currently */
        break;
	case ESP_BLUFI_EVENT_RECV_SERVER_CERT:
        /* Not handle currently */
        break;
	case ESP_BLUFI_EVENT_RECV_CLIENT_PRIV_KEY:
        /* Not handle currently */
        break;;
	case ESP_BLUFI_EVENT_RECV_SERVER_PRIV_KEY:
        /* Not handle currently */
        break;
    default:
        break;
    }
#ifdef BLE_LED_IN_CALLBACK
    gpio_set_level(LED_BLE, level);
    level = !level;
#endif
}

#define PROFILE_NUM 1
#define PROFILE_A_APP_ID 0
#define PROFILE_B_APP_ID 1
//#define PROFILE_AC_APP_ID 2
//#define PROFILE_BC_APP_ID 3

struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
    uint16_t char_handle;
    uint16_t char2_handle;
    esp_bt_uuid_t char_uuid;
    esp_bt_uuid_t char2_uuid;
    esp_gatt_perm_t perm;
    esp_gatt_char_prop_t property;
    uint16_t descr_handle;
    esp_bt_uuid_t descr_uuid;
};

#if 0
/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst gl_profile_tab[PROFILE_NUM] = {
    [PROFILE_A_APP_ID] = {
        .gatts_cb = gatts_profile_a_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
    #if 0
    [PROFILE_B_APP_ID] = {
        .gatts_cb = gatts_profile_b_event_handler,                   /* This demo does not implement, similar as profile A */
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
    #endif
};
#endif

struct gattc_profile_inst {
    esp_gattc_cb_t gattc_cb;
    uint16_t gattc_if;
    uint16_t app_id;
    uint16_t conn_id;
    esp_bd_addr_t remote_bda;
};

/* One gatt-based profile one app_id and one gattc_if, this array will store the gattc_if returned by ESP_GATTS_REG_EVT */
static struct gattc_profile_inst gl_profile_tab_c[1] = {
    [PROFILE_A_APP_ID] = {
        .gattc_cb = gattc_profile_a_event_handler,
        .gattc_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
    #if 0
    [PROFILE_B_APP_ID] = {
        .gattc_cb = gattc_profile_b_event_handler,
        .gattc_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
    #endif
};
static void gattc_profile_a_event_handler(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param)
{
    static uint32_t level = 1;
    uint16_t conn_id = 0;
    esp_ble_gattc_cb_param_t *p_data = (esp_ble_gattc_cb_param_t *)param;
    ESP_LOGW(GATTC_TAG, "Enter %s", __func__);
    switch (event) {
    case ESP_GATTC_REG_EVT:
        ESP_LOGI(GATTC_TAG, "REG_EVT");
        esp_ble_gap_set_scan_params(&ble_scan_params);
        break;
    case ESP_GATTC_OPEN_EVT:
        conn_id = p_data->open.conn_id;

        memcpy(gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda, p_data->open.remote_bda, sizeof(esp_bd_addr_t));
        ESP_LOGI(GATTC_TAG, "ESP_GATTC_OPEN_EVT conn_id %d, if %d, status %d, mtu %d", conn_id, gattc_if, p_data->open.status, p_data->open.mtu);

        ESP_LOGI(GATTC_TAG, "REMOTE BDA  %02x:%02x:%02x:%02x:%02x:%02x",
                            gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[0], gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[1], 
                            gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[2], gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[3],
                            gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[4], gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda[5]
                         );

        esp_ble_gattc_search_service(gattc_if, conn_id, NULL);
        break;
    case ESP_GATTC_SEARCH_RES_EVT: {
        esp_gatt_srvc_id_t *srvc_id = &p_data->search_res.srvc_id;
        conn_id = p_data->search_res.conn_id;
        ESP_LOGI(GATTC_TAG, "SEARCH RES: conn_id = %x", conn_id);
        if (srvc_id->id.uuid.len == ESP_UUID_LEN_16) {
            ESP_LOGI(GATTC_TAG, "UUID16: %x", srvc_id->id.uuid.uuid.uuid16);
        } else if (srvc_id->id.uuid.len == ESP_UUID_LEN_32) {
            ESP_LOGI(GATTC_TAG, "UUID32: %x", srvc_id->id.uuid.uuid.uuid32);
        } else if (srvc_id->id.uuid.len == ESP_UUID_LEN_128) {
            ESP_LOGI(GATTC_TAG, "UUID128: %x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x,%x", srvc_id->id.uuid.uuid.uuid128[0],
                     srvc_id->id.uuid.uuid.uuid128[1], srvc_id->id.uuid.uuid.uuid128[2], srvc_id->id.uuid.uuid.uuid128[3],
                     srvc_id->id.uuid.uuid.uuid128[4], srvc_id->id.uuid.uuid.uuid128[5], srvc_id->id.uuid.uuid.uuid128[6],
                     srvc_id->id.uuid.uuid.uuid128[7], srvc_id->id.uuid.uuid.uuid128[8], srvc_id->id.uuid.uuid.uuid128[9],
                     srvc_id->id.uuid.uuid.uuid128[10], srvc_id->id.uuid.uuid.uuid128[11], srvc_id->id.uuid.uuid.uuid128[12],
                     srvc_id->id.uuid.uuid.uuid128[13], srvc_id->id.uuid.uuid.uuid128[14], srvc_id->id.uuid.uuid.uuid128[15]);
        } else {
            ESP_LOGE(GATTC_TAG, "UNKNOWN LEN %d", srvc_id->id.uuid.len);
        }
        break;
    }
    case ESP_GATTC_SEARCH_CMPL_EVT:
        conn_id = p_data->search_cmpl.conn_id;
        ESP_LOGI(GATTC_TAG, "SEARCH_CMPL: conn_id = %x, status %d", conn_id, p_data->search_cmpl.status);
        esp_ble_gattc_get_characteristic(gattc_if, conn_id, &alert_service_id, NULL);
        break;
    case ESP_GATTC_GET_CHAR_EVT:
        if (p_data->get_char.status != ESP_GATT_OK) {
            break;
        }
        ESP_LOGI(GATTC_TAG, "GET CHAR: conn_id = %x, status %d", p_data->get_char.conn_id, p_data->get_char.status);
        ESP_LOGI(GATTC_TAG, "GET CHAR: srvc_id = %04x, char_id = %04x", p_data->get_char.srvc_id.id.uuid.uuid.uuid16, p_data->get_char.char_id.uuid.uuid.uuid16);

        if (p_data->get_char.char_id.uuid.uuid.uuid16 == 0x2a46) {
            ESP_LOGI(GATTC_TAG, "register notify");
            esp_ble_gattc_register_for_notify(gattc_if, gl_profile_tab_c[PROFILE_A_APP_ID].remote_bda, &alert_service_id, &p_data->get_char.char_id);
        }

        esp_ble_gattc_get_characteristic(gattc_if, conn_id, &alert_service_id, &p_data->get_char.char_id);
        break;
    case ESP_GATTC_REG_FOR_NOTIFY_EVT: {
        uint16_t notify_en = 1;
        ESP_LOGI(GATTC_TAG, "REG FOR NOTIFY: status %d", p_data->reg_for_notify.status);
        ESP_LOGI(GATTC_TAG, "REG FOR_NOTIFY: srvc_id = %04x, char_id = %04x", p_data->reg_for_notify.srvc_id.id.uuid.uuid.uuid16, p_data->reg_for_notify.char_id.uuid.uuid.uuid16);

        esp_ble_gattc_write_char_descr(
                gattc_if,
                conn_id,
                &alert_service_id,
                &p_data->reg_for_notify.char_id,
                &notify_descr_id,
                sizeof(notify_en),
                (uint8_t *)&notify_en,
                ESP_GATT_WRITE_TYPE_RSP,
                ESP_GATT_AUTH_REQ_NONE);
        break;
    }
    case ESP_GATTC_NOTIFY_EVT:
        ESP_LOGI(GATTC_TAG, "NOTIFY: len %d, value %08x", p_data->notify.value_len, *(uint32_t *)p_data->notify.value);
        break;
    case ESP_GATTC_WRITE_DESCR_EVT:
        ESP_LOGI(GATTC_TAG, "WRITE: status %d", p_data->write.status);
        break;
    case ESP_GATTC_SRVC_CHG_EVT: {
        esp_bd_addr_t bda;
        memcpy(bda, p_data->srvc_chg.remote_bda, sizeof(esp_bd_addr_t));
        ESP_LOGI(GATTC_TAG, "ESP_GATTC_SRVC_CHG_EVT, bd_addr:%08x%04x",(bda[0] << 24) + (bda[1] << 16) + (bda[2] << 8) + bda[3],
                 (bda[4] << 8) + bda[5]);
        break;
    }
    default:
        break;
    }
#ifdef BLE_LED_IN_CALLBACK
    gpio_set_level(LED_BLE, level);
    level = !level;
#endif
}

#if 0
static void gatts_profile_a_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) 
{
    static uint32_t level = 1;
    switch (event) {
    case ESP_GATTS_REG_EVT:
        ESP_LOGI(GATTS_TAG, "REGISTER_APP_EVT, status %d, app_id %d\n", param->reg.status, param->reg.app_id);
        gl_profile_tab[PROFILE_A_APP_ID].service_id.is_primary = true;
        gl_profile_tab[PROFILE_A_APP_ID].service_id.id.inst_id = 0x00;
        gl_profile_tab[PROFILE_A_APP_ID].service_id.id.uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab[PROFILE_A_APP_ID].service_id.id.uuid.uuid.uuid16 = GATTS_SERVICE_UUID_TEST_A;

        esp_ble_gap_set_device_name(gBLEDeviceName);
#ifdef CONFIG_SET_RAW_ADV_DATA
        esp_ble_gap_config_adv_data_raw(dione_raw_adv_data, sizeof(dione_raw_adv_data));
        esp_ble_gap_config_scan_rsp_data_raw(dione_raw_scan_rsp_data, sizeof(dione_raw_scan_rsp_data));
#else
        esp_ble_gap_config_adv_data(&dione_adv_data);
#endif
        esp_ble_gatts_create_service(gatts_if, &gl_profile_tab[PROFILE_A_APP_ID].service_id, GATTS_NUM_HANDLE_TEST_A);
        break;
    case ESP_GATTS_READ_EVT: {
        ESP_LOGI(GATTS_TAG, "GATT_READ_EVT, conn_id %d, trans_id %d, handle %d\n", param->read.conn_id, param->read.trans_id, param->read.handle);
        esp_gatt_rsp_t rsp;
        memset(&rsp, 0, sizeof(esp_gatt_rsp_t));
        rsp.attr_value.handle = param->read.handle;
        rsp.attr_value.len = 4;
        rsp.attr_value.value[0] = 0xde;
        rsp.attr_value.value[1] = 0xed;
        rsp.attr_value.value[2] = 0xbe;
        rsp.attr_value.value[3] = 0xef;
        esp_ble_gatts_send_response(gatts_if, param->read.conn_id, param->read.trans_id,
                                    ESP_GATT_OK, &rsp);
        break;
    }
    case ESP_GATTS_WRITE_EVT: {
        ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle %d\n", param->write.conn_id, param->write.trans_id, param->write.handle);
        ESP_LOGI(GATTS_TAG, "GATT_WRITE_EVT, value len %d, value %08x\n", param->write.len, *(uint32_t *)param->write.value);
        esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
        break;
    }
    case ESP_GATTS_EXEC_WRITE_EVT:
    case ESP_GATTS_MTU_EVT:
    case ESP_GATTS_CONF_EVT:
    case ESP_GATTS_UNREG_EVT:
        break;
    case ESP_GATTS_CREATE_EVT:
        ESP_LOGI(GATTS_TAG, "CREATE_SERVICE_EVT, status %d,  service_handle %d\n", param->create.status, param->create.service_handle);
        gl_profile_tab[PROFILE_A_APP_ID].service_handle = param->create.service_handle;
        gl_profile_tab[PROFILE_A_APP_ID].char_uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab[PROFILE_A_APP_ID].char_uuid.uuid.uuid16 = GATTS_CHAR_UUID_TEST_A;
        gl_profile_tab[PROFILE_A_APP_ID].char2_uuid.len = ESP_UUID_LEN_16;
        gl_profile_tab[PROFILE_A_APP_ID].char2_uuid.uuid.uuid16 = GATTS_CHAR2_UUID_TEST_A;

        esp_ble_gatts_add_char(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &gl_profile_tab[PROFILE_A_APP_ID].char_uuid,
                               ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
                               ESP_GATT_CHAR_PROP_BIT_NOTIFY, 
                               &gatts_demo_char1_val, NULL);
        esp_ble_gatts_add_char(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &gl_profile_tab[PROFILE_A_APP_ID].char2_uuid,
                               ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
                               ESP_GATT_CHAR_PROP_BIT_WRITE, 
                               &gatts_demo_char2_val, NULL);
        esp_ble_gatts_start_service(gl_profile_tab[PROFILE_A_APP_ID].service_handle);
        break;
    case ESP_GATTS_ADD_INCL_SRVC_EVT:
        break;
    case ESP_GATTS_ADD_CHAR_EVT: {
        uint16_t length = 0;
        const uint8_t *prf_char;
        uint16_t char_uuid = param->add_char.char_uuid.uuid.uuid16;
        ESP_LOGI(GATTS_TAG, "ADD_CHAR_EVT, status 0x%x,  attr_handle %d, service_handle %d, char_uuid:0x%X\n",
                param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle, char_uuid);
        if (char_uuid == GATTS_CHAR_UUID_TEST_A)
        {
            gl_profile_tab[PROFILE_A_APP_ID].char_handle = param->add_char.attr_handle;
            gl_profile_tab[PROFILE_A_APP_ID].descr_uuid.len = ESP_UUID_LEN_16;
            gl_profile_tab[PROFILE_A_APP_ID].descr_uuid.uuid.uuid16 = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;
            #if 0
            esp_ble_gatts_get_attr_value(param->add_char.attr_handle,  &length, &prf_char);
            ESP_LOGI(GATTS_TAG, "the gatts demo char length = %x\n", length);
            for(int i = 0; i < length; i++){
                ESP_LOGI(GATTS_TAG, "prf_char[%x] =%x\n",i,prf_char[i]);
            }
            #endif
            esp_ble_gatts_add_char_descr(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &gl_profile_tab[PROFILE_A_APP_ID].descr_uuid,
                                         ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, NULL, NULL);
        }
        else if (char_uuid == GATTS_CHAR2_UUID_TEST_A)
        {
            esp_ble_gatts_add_char_descr(gl_profile_tab[PROFILE_A_APP_ID].service_handle, &gl_profile_tab[PROFILE_A_APP_ID].descr_uuid,
                                         ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE, NULL, NULL);
        }
        break;
    }
    case ESP_GATTS_ADD_CHAR_DESCR_EVT:
        ESP_LOGI(GATTS_TAG, "ADD_DESCR_EVT, status %d, attr_handle %d, service_handle %d\n",
                 param->add_char.status, param->add_char.attr_handle, param->add_char.service_handle);
        break;
    case ESP_GATTS_DELETE_EVT:
        break;
    case ESP_GATTS_START_EVT:
        ESP_LOGI(GATTS_TAG, "SERVICE_START_EVT, status %d, service_handle %d\n",
                 param->start.status, param->start.service_handle);
        break;
    case ESP_GATTS_STOP_EVT:
        break;
    case ESP_GATTS_CONNECT_EVT:
        ESP_LOGI(GATTS_TAG, "ESP_GATTS_CONNECT_EVT, conn_id %d, remote %02x:%02x:%02x:%02x:%02x:%02x:, is_conn %d\n",
                 param->connect.conn_id,
                 param->connect.remote_bda[0], param->connect.remote_bda[1], param->connect.remote_bda[2],
                 param->connect.remote_bda[3], param->connect.remote_bda[4], param->connect.remote_bda[5],
                 param->connect.is_connected);
        gl_profile_tab[PROFILE_A_APP_ID].conn_id = param->connect.conn_id;
        break;
    case ESP_GATTS_DISCONNECT_EVT:
        esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_GATTS_OPEN_EVT:
    case ESP_GATTS_CANCEL_OPEN_EVT:
    case ESP_GATTS_CLOSE_EVT:
    case ESP_GATTS_LISTEN_EVT:
    case ESP_GATTS_CONGEST_EVT:
    default:
        break;
    }
    gpio_set_level(LED_BLE, level);
    level = !level;
}
#endif

void dumpBytes(const uint8_t* data, size_t count)
{
    for (uint32_t i = 0; i < count; ++i) {
        if (i % 32 == 0) {
            printf("%08x    ", i);
        }
        printf("%02x ", data[i]);
        if ((i + 1) % 32 == 0) {
            printf("\n");
        }
    }
}

#ifdef MQTT_SERVER_DIONE
static void parse_adv(uint8_t *pAdv, uint8_t len, int rssi)
{
    //ESP_BD_ADDR_STR;
    char temp[16] = {0};
    char *buf = malloc(128);
    memset(buf, 0, 128);
    uint32_t tick = xTaskGetTickCount();
    sprintf(temp, "[%d] ", tick);
    strcat(buf, temp);
    for (int i = 0; i < len; i++)
    {
        sprintf(temp, "%02X", pAdv[i]);
        strcat(buf, temp);
    }
    sprintf(temp, " [%d]", rssi);
    strcat(buf, temp);
    mqtt_publish(g_mqtt_client, "Dione/adv", buf, strlen(buf), 0, 0);
    free(buf);
}
#elif defined(MQTT_SERVER_ONENET)
static void parse_adv(uint8_t *pAdv, uint8_t len, int rssi)
{
}
#elif defined(MQTT_SERVER_MICRODUINO)
static void parse_adv(uint8_t *pAdv, uint8_t len, int rssi)
{
    char temp[16] = {0};
    char *buf = malloc(128);
    memset(buf, 0, 128);
    for (int i = 0; i < len; i++)
    {
        sprintf(temp, "%02X", pAdv[i]);
        strcat(buf, temp);
    }
    char *mqtt_buf = malloc(256);
    memset(mqtt_buf, 0, 256);
    sprintf(mqtt_buf, "{\"adv\":\"%s\",\"RSSI\":%d}", buf, rssi);
    mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", mqtt_buf, strlen(mqtt_buf), 0, 0);
    free(buf);
    free(mqtt_buf);
}
#elif defined(MQTT_SERVER_SH_HUOWEI)
static inline bool is_iBeacon(uint8_t *pAdv, uint8_t len)
{
    uint8_t magincLen = len - 6;
    char iBeaconMagic[16]={0xff, 0x4c, 0x00, 0x02, 0x15};
    if (len <= 26)
    {
        return false;
    }
    for (int i = 0; i < magincLen; i++)
    {
        if (memcmp(pAdv+i, iBeaconMagic, 5) == 0)
        {
            return true;
        }
    }
    return false;
}

static void parse_adv(uint8_t *pAdv, uint8_t len, int rssi, esp_bd_addr_t addr)
{
    uint8_t battery = 100;
    char temp[16] = {0};
    if (gJB == true)
    {
        if (is_iBeacon(pAdv, len) == false)
        {
            ESP_LOGW(TAG_DIONE, "Not iBeacon, just return.");
            return;
        }
    }
    char *buf = malloc(128);
    memset(buf, 0, 128);
    for (int i = 0; i < len; i++)
    {
        sprintf(temp, "%02X", pAdv[i]);
        strcat(buf, temp);
    }
    char *mqtt_buf = malloc(1024);
    //memset(mqtt_buf, 0, 1024);
    battery = (float)gADCvalue / (float)VOLTAGE_5V_ADC_VALUE * 100.0;
    #if 0
    sprintf(mqtt_buf, "{\"seq\":%llu,\"adv\":\"%s\",\"RSSI\":%d}", gSeq++, buf, rssi);
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_DATA, mqtt_buf, strlen(mqtt_buf), 0, 0);
    #else
    sprintf(mqtt_buf, "{\"cid\":\"%s\",\"seq\":%llu,\"time\":%lu,\"adv\":\"%s\",\"adv_mac\":\"%02X%02X%02X%02X%02X%02X\",\"battery\":\"%d%%\",\"RSSI\":%d}", 
                      gCID, gSeq++, time(NULL), buf, ESP_BD_ADDR_HEX(addr), battery, rssi);
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_DATA, mqtt_buf, strlen(mqtt_buf), 0, 0);
    #endif
    free(buf);
    free(mqtt_buf);
    gScanTime = xTaskGetTickCount() * portTICK_PERIOD_MS;
}
#endif

void dione_gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    static uint32_t level = 0;
    ESP_LOGW(TAG_DIONE, "Enter %s, event=%d", __func__, event);
    uint8_t *adv_name = NULL;
    uint8_t adv_name_len = 0;
    switch (event) {
    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
        //esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
        //esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT:
        //esp_ble_gap_start_advertising(&dione_adv_params);
        if (gBLEServer == 2)
        {
            xEventGroupSetBits(dione_event_group, DIONE_EVENT_ADV_SCAN);
        }
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTS_TAG, "Advertising start failed\n");
        }
        else
        {
            ESP_LOGE(GATTS_TAG, "Advertising start success\n");
        }
        break;
    case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
        if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTS_TAG, "Advertising stop failed\n");
        }
        else {
            ESP_LOGI(GATTS_TAG, "Stop adv successfully\n");
        }
        if (gBLEServer == 2)
        {
            xEventGroupSetBits(dione_event_group, DIONE_EVENT_ADV_SCAN);
        }
        break;

    case ESP_GAP_BLE_SCAN_PARAM_SET_COMPLETE_EVT: {
        //the unit of the duration is second
        if (gBLEServer == 0)
        {
            uint32_t duration = 4294967295;
            esp_ble_gap_start_scanning(duration);
        }
        else
        {
        //xEventGroupSetBits(dione_event_group, DIONE_EVENT_ADV_SCAN);
        }
        break;
    }
    case ESP_GAP_BLE_SCAN_START_COMPLETE_EVT:
        //scan start complete event to indicate scan start successfully or failed
        if (param->scan_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
            ESP_LOGE(GATTC_TAG, "Scan start failed");
        }
        else
        {
            ESP_LOGE(GATTC_TAG, "Scan start sucess!");
        }

        break;
    case ESP_GAP_BLE_SCAN_RESULT_EVT: {
        esp_ble_gap_cb_param_t *scan_result = (esp_ble_gap_cb_param_t *)param;
        ESP_LOGW(GATTC_TAG, "search_evt = %d", scan_result->scan_rst.search_evt);
        switch (scan_result->scan_rst.search_evt) {
        case ESP_GAP_SEARCH_INQ_RES_EVT:
            if (gMQTTConnected == false)
                break;
            #if 0
            for (int i = 0; i < 6; i++) {
                //ESP_LOGI(GATTC_TAG, "%x:", scan_result->scan_rst.bda[i]);
                printf("%x:", scan_result->scan_rst.bda[i]);
            }
            printf("\r\n");
            #endif
            ESP_LOGI(GATTC_TAG, "Searched Adv Data Len %d, Scan Response Len %d\n", scan_result->scan_rst.adv_data_len, scan_result->scan_rst.scan_rsp_len);
            ESP_LOGI(GATTC_TAG, "dev_type = %d, ble_addr_type = %d, ble_evt_type=%d, flag=%d, num_resps = %d", 
                                scan_result->scan_rst.dev_type, 
                                scan_result->scan_rst.ble_addr_type, 
                                scan_result->scan_rst.ble_evt_type,
                                scan_result->scan_rst.flag,
                                scan_result->scan_rst.num_resps);
            #if 0
            dumpBytes(scan_result->scan_rst.ble_adv, ESP_BLE_ADV_DATA_LEN_MAX + ESP_BLE_SCAN_RSP_DATA_LEN_MAX);
            
            adv_name = esp_ble_resolve_adv_data(scan_result->scan_rst.ble_adv,
                                                ESP_BLE_AD_TYPE_NAME_CMPL, &adv_name_len);
            
            ESP_LOGI(GATTC_TAG, "Searched Device Name Len %d", adv_name_len);
            for (int j = 0; j < adv_name_len; j++) {
                //ESP_LOGI(GATTC_TAG, "%c", adv_name[j]);
                printf("%c", adv_name[j]);
            }
            
            printf("\r\n");
            #endif
            if (gbIfEverScanAdv == false)
            {
                gbIfEverScanAdv = true;
            }
            parse_adv(scan_result->scan_rst.ble_adv, scan_result->scan_rst.adv_data_len, scan_result->scan_rst.rssi, scan_result->scan_rst.bda);
            #if 0
            if (adv_name != NULL) {
                if (strlen(device_name) == adv_name_len && strncmp((char *)adv_name, device_name, adv_name_len) == 0) {
                    ESP_LOGI(GATTC_TAG, "Searched device %s\n", device_name);
                    if (connect == false) {
                        connect = true;
                        ESP_LOGI(GATTC_TAG, "Connect to the remote device.");
                        esp_ble_gap_stop_scanning();
                        esp_ble_gattc_open(gl_profile_tab_c[PROFILE_A_APP_ID].gattc_if, scan_result->scan_rst.bda, true);
                        //esp_ble_gattc_open(gl_profile_tab_c[PROFILE_B_APP_ID].gattc_if, scan_result->scan_rst.bda, true);
                    }
                }
            }
            #endif
            break;
        case ESP_GAP_SEARCH_INQ_CMPL_EVT:
            if (gBLEServer == 0)
            {
                //esp_ble_gap_start_scanning(5);
            }
            else
            {
                xEventGroupSetBits(dione_event_group, DIONE_EVENT_ADV_SCAN);
            }
            break;
        default:
            break;
        }
        break;
    }

    case ESP_GAP_BLE_SCAN_STOP_COMPLETE_EVT:
        if (param->scan_stop_cmpl.status != ESP_BT_STATUS_SUCCESS){
            ESP_LOGE(GATTC_TAG, "Scan stop failed");
        }
        else {
            ESP_LOGI(GATTC_TAG, "Stop scan successfully");
        }
        if (gBLEServer == 0)
        {
            //esp_ble_gap_start_scanning(5);
        }
        else if (gBLEServer == 2)
        {
            xEventGroupSetBits(dione_event_group, DIONE_EVENT_ADV_SCAN);
        }
        break;
    default:
        break;
    }
#ifdef BLE_LED_IN_CALLBACK
    gpio_set_level(LED_BLE, level);
    level = !level;
#endif
}

static void dione_init_uart(void)
{
    const int uart_num = UART_NUM_1;
    uart_config_t uart_config = {
        .baud_rate = 57600,
        //.baud_rate = 38400,
        //.baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .rx_flow_ctrl_thresh = 122,
    };
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    //Configure UART1 parameters
    uart_param_config(uart_num, &uart_config);
    //Set UART log level
    //esp_log_level_set(TAG_DIONE, ESP_LOG_VERBOSE);
    //Set UART1 pins(TX: IO4, RX: I05)
    uart_set_pin(uart_num, DIONE_UART_TXD, DIONE_UART_RXD, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    //Install UART driver (we don't need an event queue here)
    //In this example we don't even use a buffer for sending data.
    uart_driver_install(uart_num, UART_BUF_SIZE * 2, 0, 0, NULL, 0);
}

static void dione_BLE_adv_led_task(void *pvParameters)
{
}

static void dione_BLE_scan_led_task(void *pvParameters)
{
}

static bool bAdv = true;
static void dione_BLE_led_task(void *pvParameters)
{
    //static uint32_t level = 1;
    uint32_t msServer = dione_adv_params.adv_int_max * 0.625;
    uint32_t msClient = ble_scan_params.scan_interval * 0.625;
    gpio_num_t led_num = LED_BLE;
    while (1)
    {
        gpio_set_level(led_num, LED_ON);
        vTaskDelay(50/portTICK_PERIOD_MS);
        gpio_set_level(led_num, LED_OFF);
        if (gBLEServer == 2)
        {
            if (!bAdv)// Adv led
            {
                gpio_set_level(LED_UWB, LED_OFF);
                led_num = LED_BLE;
                //vTaskDelay(msServer/portTICK_PERIOD_MS);
                vTaskDelay(500/portTICK_PERIOD_MS);
            }
            else// Scan led
            {
                gpio_set_level(LED_BLE, LED_OFF);
                led_num = LED_UWB;
                //vTaskDelay(msClient/portTICK_PERIOD_MS);
                vTaskDelay(1000/portTICK_PERIOD_MS);
            }
        }
        else if (gBLEServer == 1) // This is a server
        {
            vTaskDelay(msServer/portTICK_PERIOD_MS);
        }
        else // This is client
        {
            vTaskDelay(3000/portTICK_PERIOD_MS);
        }
    }
}

// Maybe name it UWB task?
static void dione_uart_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    uint32_t level = 0;
    uint8_t* data = (uint8_t*) malloc(UART_BUF_SIZE);
    memset(data, 0, UART_BUF_SIZE);
    //char beginString[12] = {0x0D, 0x0A, 0x0D, 0x3E}; // 0x3E : ">"
#if 0
    //等待远端MQTT指令
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_UWB_START,
                            true, false, 30000/portTICK_PERIOD_MS);
#else
    uart_flush(UART_NUM_1);
    //vTaskDelay(100/portTICK_PERIOD_MS);
    strncpy((char*)data, "daps\r\n", 6);
    uart_write_bytes(UART_NUM_1, (const char*)data, 6);
    vTaskDelay(100/portTICK_PERIOD_MS);
    uart_write_bytes(UART_NUM_1, (const char*)data, 6);
    
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                            false, false, portMAX_DELAY);

    uart_flush(UART_NUM_1);
    strncpy((char*)data, gDapsCommand, sizeof(gDapsCommand) - 1);
    uart_write_bytes(UART_NUM_1, (const char*)data, strnlen((char*)data, 32));
    vTaskDelay(100/portTICK_PERIOD_MS);
    uart_write_bytes(UART_NUM_1, (const char*)data, strnlen((char*)data, 32));
#endif
    while (1) {
        //ESP_LOGW(TAG_DIONE, "hehe %d", level);
        int len = uart_read_bytes(UART_NUM_1, data, UART_BUF_SIZE, 20 / portTICK_RATE_MS);
        if (len > 0)
        {
            gpio_set_level(LED_UWB, level);
            #if 0
            ESP_LOGI(TAG_DIONE, "uart read : %d", len);
            for (int i = 0; i < len; i++)
            {
                printf("0x%x ", data[i]);
            }
            printf("\r\n");
            #else
#if 0
            if (len == 4)
            {
                if (memcmp(beginString, data, 4) == 0)
                {
                    strncpy((char*)data, "daps 0\r\n", 8);
                    uart_write_bytes(uart_num, (const char*)data, 8);
                    memset(data, 0, 6);
                    continue;
                }
            }
            else
#endif
            {
                //ESP_LOGI(TAG1, "%s", data);
                //printf("%s\r\n", data);
                mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_UWB, (char*)data, len, 0, 0);
                //memset(data, 0, len+2);
            }
            level = !level;
            #endif
        }
    }
}

/* Save the number of module restarts in NVS
   by first reading and then incrementing
   the number that has been saved previously.
   Return an error if anything goes wrong
   during this process.
 */
static esp_err_t dione_save_restart_counter(void)
{
    nvs_handle my_handle;
    esp_err_t err;

    g_restart_counter = 0;
    // Open
    err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;

    // Read
    //int32_t restart_counter = 0; // value will default to 0, if not set yet in NVS
    err = nvs_get_i32(my_handle, "restart_conter", &g_restart_counter);
    if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) return err;

    // Write
    g_restart_counter++;
    err = nvs_set_i32(my_handle, "restart_conter", g_restart_counter);
    if (err != ESP_OK) return err;

    // Commit written value.
    // After setting any values, nvs_commit() must be called to ensure changes are written
    // to flash storage. Implementations may write to storage at other times,
    // but this is not guaranteed.
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);
    return ESP_OK;
}
static void dione_obtain_time(void)
{
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_STA_GOT_IP,
                        false, false, portMAX_DELAY);
    sntp_setoperatingmode(SNTP_OPMODE_POLL);
    //sntp_setservername(0, "pool.ntp.org");
    sntp_setservername(0, "cn.pool.ntp.org");
    sntp_init();

    // wait for time to be set
    time_t now = 0;
    struct tm timeinfo = { 0 };
    int retry = 0;
    const int retry_count = 200;
    while(timeinfo.tm_year < (2017 - 1900) && ++retry < retry_count) {
        ESP_LOGI(TAG_DIONE, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
        time(&now);
        localtime_r(&now, &timeinfo);
    }
}

static void getTime(void *pvParameters)
{
    #if 1
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    
    time_t now;
    struct tm timeinfo;
    time(&now);
    localtime_r(&now, &timeinfo);
    if (pvParameters != NULL)
    {
        dione_obtain_time();
        // update 'now' variable with current time
        time(&now);
    }
    // Is time set? If not, tm_year will be (1970 - 1900).
    else if (timeinfo.tm_year < (2017 - 1900)) {
        ESP_LOGI(TAG_DIONE, "Time is not set yet. Connecting to WiFi and getting time over NTP.");
        dione_obtain_time();
        // update 'now' variable with current time
        time(&now);
    }
    char strftime_buf[64];
    // Set timezone to China Standard Time
    //setenv("TZ", "CST-8CDT-9,M4.2.0/2,M9.2.0/3", 1);
    setenv("TZ", "GMT-8", 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGI(TAG_DIONE, "The current date/time in BeiJing is: %s", strftime_buf);
    sntp_stop();
    //vTaskDelay(2000 / portTICK_PERIOD_MS);
    char msg[128] = {0};
    if (gManualRestart == 1) // Manual reset.
    {
        gManualRestart = 0;
        dione_nvs_save_manual_restart();
        vTaskDelay(2000 / portTICK_PERIOD_MS);

        ESP_LOGI(TAG_DIONE, "Let us sync time...");
        dione_obtain_time();
        time(&now);
        xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
        sprintf(msg, "OH, this is manmual reset: %s...............................................\r\n", strftime_buf);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)msg, strlen(msg), 0, 0);
        vTaskDelete(NULL);
        return;
    }
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
    if (pvParameters == NULL)
    {
        sprintf(msg, " system started : %s\r\n", strftime_buf);
    }
    else
    {
        sprintf(msg, " Current time : %s\r\n", strftime_buf);
    }
    mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)msg, strlen(msg), 0, 0);
    vTaskDelete(NULL);
    #endif
}

#if 0
static void dione_gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            //gl_profile_tab[param->reg.app_id].gatts_if = gatts_if;
            gl_profile_tab[PROFILE_A_APP_ID].gatts_if = gatts_if;
        } else {
            ESP_LOGI(GATTS_TAG, "Reg app failed, app_id %04x, status %d\n",
                    param->reg.app_id, 
                    param->reg.status);
            return;
        }
    }

    /* If the gatts_if equal to profile A, call profile A cb handler,
     * so here call each profile's callback */
    do {
        int idx;
        for (idx = 0; idx < PROFILE_NUM; idx++) {
            if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gatts_if == gl_profile_tab[idx].gatts_if) {
                if (gl_profile_tab[idx].gatts_cb) {
                    gl_profile_tab[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    } while (0);
}
#endif

static void dione_gattc_event_handler(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param)
{
    ESP_LOGW(GATTC_TAG, "Enter %s", __func__);
    ESP_LOGI(GATTC_TAG, "EVT %d, gattc if %d", event, gattc_if);

    /* If event is register event, store the gattc_if for each profile */
    if (event == ESP_GATTC_REG_EVT) {
        if (param->reg.status == ESP_GATT_OK) {
            gl_profile_tab_c[PROFILE_A_APP_ID].gattc_if = gattc_if;
        } else {
            ESP_LOGI(GATTC_TAG, "Reg app failed, app_id %04x, status %d",
                    param->reg.app_id, 
                    param->reg.status);
            return;
        }
    }

    /* If the gattc_if equal to profile A, call profile A cb handler,
     * so here call each profile's callback */
    do {
        int idx;
        for (idx = 0; idx < PROFILE_NUM; idx++) {
            if (gattc_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gattc_if == gl_profile_tab_c[idx].gattc_if) {
                if (gl_profile_tab_c[idx].gattc_cb) {
                    gl_profile_tab_c[idx].gattc_cb(event, gattc_if, param);
                }
            }
        }
    } while (0);
}

static void dione_heartbeat_task(void *pvParameters)
{
    char strftime_buf[128];
    time_t now;
    struct tm timeinfo;
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
    #if 0
    while (1) {
        time(&now);
        localtime_r(&now, &timeinfo);
        strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
        //ESP_LOGI(TAG_DIONE, "The current date/time in BeiJing is: %s", strftime_buf);
        mqtt_publish(g_mqtt_client, "Dione/Time", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
    #else
    while (true)
    {
        gpio_set_level(LED_UWB, LED_ON);
        
        // 1 tick is 10ms.
        uint32_t c = xTaskGetTickCount();
        uint32_t ms = c * portTICK_PERIOD_MS;
        float run_second = ms / 1000.0;
        float run_minute = run_second / 60.0;
        float run_hours = run_minute / 60.0;
        #ifdef MQTT_SERVER_DIONE
        sprintf(strftime_buf, "Tick[%d], %.2fs, %.2fm, %.2fh", c, run_second, run_minute, run_hours);
        mqtt_publish(g_mqtt_client, "Dione/Time", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        #elif defined(MQTT_SERVER_ONENET)
        
        #elif defined(MQTT_SERVER_MICRODUINO)
        sprintf(strftime_buf, "{\"run_second\":%.2f,\"run_minute\":%.2f,\"run_hours\":%.2f}", run_second, run_minute, run_hours);
        ESP_LOGW(TAG_DIONE, "%s", strftime_buf);
        mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        //sprintf(strftime_buf, "{\"loc\":\"%f,%f\"}", 116.33, 40.12);
        strncpy(strftime_buf, "{\"loc\":\"116.3448658,40.008828799999996\"}", 127);
        mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
#elif defined(MQTT_SERVER_SH_HUOWEI)
        sprintf(strftime_buf, "{\"run_second\":%.2f,\"run_minute\":%.2f,\"run_hours\":%.2f}", run_second, run_minute, run_hours);
        ESP_LOGW(TAG_DIONE, "%s", strftime_buf);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        vTaskDelay(3000 / portTICK_PERIOD_MS);
        //sprintf(strftime_buf, "{\"loc\":\"%f,%f\"}", 116.33, 40.12);
        strncpy(strftime_buf, "{\"loc\":[116.3448658,40.008828799999996]}", 127);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)strftime_buf, strlen(strftime_buf), 0, 0);
#endif
        vTaskDelay(50 / portTICK_PERIOD_MS);
        gpio_set_level(LED_UWB, LED_OFF);
        vTaskDelay(15000 / portTICK_PERIOD_MS);
    }
    #endif
}

//static const char *json = "{\"x\":20.5}";
static const char *json = " \
    { \
	\"cid\": \"D067E51EF717\", \
	\"seq\": 1, \
	\"cmd\": 4, \
	\"msg\": { \
		\"ibeacon\": { \
			\"base\": { \
				\"interval\": 1 \
			}, \
			\"tag\": { \
				\"uuid\": \"AABBCCDDEEFF00112233445566778899\", \
				\"major\": \"FFFF\", \
				\"minor\": \"FFFF\" \
			} \
		} \
	} \
} \
    ";

static void dione_test_json_task(void *pvParameters)
{
    float run_second = 60.34;
    float run_minute = run_second / 60.0;
    float run_hours = run_minute / 60.0;
    //char json[128];
    float parsedFloat;
    char mqtt_buf[128];
    
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    jsmn_init(&json_parser);
    while (true)
    {
        //sprintf(json, "{\"run_second\":%.2f,\"run_minute\":%.2f,\"run_hours\":%.2f}", run_second, run_minute, run_hours);
        //ESP_LOGW(TAG_DIONE, "%s", strftime_buf);
        int r = jsmn_parse(&json_parser, json, strlen(json), token, sizeof(token) / sizeof(token[0]));
    	IoT_Error_t rc = parseFloatValue(&parsedFloat, json, token + 2);
        ESP_LOGW(TAG_DIONE, "rc = %d, r = %d", rc, r);
        for (int i = 0; i < r; i++)
        {
            printf("[%d]: type=%d, start=%d, end=%d, size=%d\r\n", i, token[i].type, token[i].start, token[i].end, token[i].size);
        }

        sprintf(mqtt_buf, "{\"parsedValue\":%.2f}", parsedFloat);
        mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)mqtt_buf, strlen(mqtt_buf), 0, 0);
        vTaskDelay(3100 / portTICK_PERIOD_MS);
    }
}

static void dione_restart_task(void *pvParameters)
{
    int minute_count = 0;
    while (true)
    {
        vTaskDelay(60000 / portTICK_PERIOD_MS);
        minute_count++;
        if (minute_count >= 1440) // 24 * 60
        {
            if (gDioneState == 1)
            {
                continue;
            }
            dione_nvs_save_seq();
            vTaskDelay(3000 / portTICK_PERIOD_MS);
            gManualRestart = 1;
            dione_nvs_save_manual_restart();
            vTaskDelay(3000 / portTICK_PERIOD_MS);
            esp_restart();
        }
    }
}


static void dione_scan_monitor_task(void *pvParameters)
{
    int minute_count = 0;
    uint32_t curMS;
    while (true)
    {
        vTaskDelay(60000 / portTICK_PERIOD_MS);
        if (!gbIfEverScanAdv) continue;
        curMS = xTaskGetTickCount() * portTICK_PERIOD_MS;
        if (curMS - gScanTime > 300000) // 如果5分钟都没扫描到广播，说明蓝牙协议栈坏掉了
        {
            if (gDioneState == 1)
            {
                continue;
            }
            dione_nvs_save_seq();
            vTaskDelay(3000 / portTICK_PERIOD_MS);
            gManualRestart = 1;
            dione_nvs_save_manual_restart();
            vTaskDelay(3000 / portTICK_PERIOD_MS);
            esp_restart();
        }
    }
}

static void dione_BLE_adv_scan_task(void *pvParameters)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    while (true)
    {
        if (bAdv)
        {
            ESP_LOGE(TAG_DIONE, "let's wait for adv.");
        }
        else
        {
            ESP_LOGE(TAG_DIONE, "let's wait for scan.");
        }
        xEventGroupWaitBits(dione_event_group, DIONE_EVENT_ADV_SCAN,
                        true, false, portMAX_DELAY);
        if (gDioneState == 1)
        {
            break;
        }
        if (bAdv)
        {
            bAdv = false;
            esp_ble_gap_start_advertising(&dione_adv_params);
            vTaskDelay(3000 / portTICK_PERIOD_MS);
            esp_ble_gap_stop_advertising();
        }
        else
        {
            bAdv = true;
            esp_ble_gap_start_scanning(5);
        }
    }
    vTaskDelete(NULL);
}

// Handle of the wear levelling library instance
static wl_handle_t s_wl_handle = WL_INVALID_HANDLE;
// Mount path for the partition
const char *base_path = "/spiflash";
static void testFAT(void)
{
    ESP_LOGE(TAG_DIONE, "Mounting FAT filesystem");
    // To mount device we need name of device partition, define base_path
    // and allow format partition in case if it is new one and was not formated before
    const esp_vfs_fat_mount_config_t mount_config = {
            .max_files = 4,
            .format_if_mount_failed = true
    };
    esp_err_t err = esp_vfs_fat_spiflash_mount(base_path, "storage", &mount_config, &s_wl_handle);
    if (err != ESP_OK) {
        ESP_LOGE(TAG_DIONE, "Failed to mount FATFS (0x%x)", err);
        return;
    }
    ESP_LOGI(TAG_DIONE, "Opening file");
    FILE *f = fopen("/spiflash/hello.txt", "wb");
    if (f == NULL) {
        ESP_LOGE(TAG_DIONE, "Failed to open file for writing");
        return;
    }
    fprintf(f, "written using ESP-IDF %s\n", esp_get_idf_version());
    fclose(f);
    ESP_LOGI(TAG_DIONE, "File written");

    // Open file for reading
    ESP_LOGI(TAG_DIONE, "Reading file");
    f = fopen("/spiflash/hello.txt", "rb");
    if (f == NULL) {
        ESP_LOGE(TAG_DIONE, "Failed to open file for reading");
        return;
    }
    char line[128];
    fgets(line, sizeof(line), f);
    fclose(f);
    // strip newline
    char *pos = strchr(line, '\n');
    if (pos) {
        *pos = '\0';
    }
    ESP_LOGI(TAG_DIONE, "Read from file: '%s'", line);

    // Unmount FATFS
    ESP_LOGI(TAG_DIONE, "Unmounting FAT filesystem");
    ESP_ERROR_CHECK( esp_vfs_fat_spiflash_unmount(base_path, s_wl_handle));

    ESP_LOGI(TAG_DIONE, "Done");
}


static esp_err_t dione_nvs_save_UM100(uint8_t haveUM100)
{
    nvs_handle my_handle;
    gHaveUM100 = haveUM100;
    // Open
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "UWB.UM", gHaveUM100);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_bleServer(uint8_t ble)
{
    nvs_handle my_handle;
    gBLEServer = ble;
    // Open
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "ble_server", gBLEServer);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ble_name(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.name", gBLEDeviceName, sizeof(gBLEDeviceName));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_sta_ssid(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.sta.ssid", gSTA_ssid, sizeof(gSTA_ssid));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_sta_pass(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.sta.pass", gSTA_pass, sizeof(gSTA_pass));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ap_ssid(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.ap.ssid", gAP_ssid, sizeof(gAP_ssid));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ap_pass(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.ap.pass", gAP_pass, sizeof(gAP_pass));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_manual_restart(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "dione.mr", gManualRestart);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ap_max_conn_num(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "dione.ap.mc", gAP_max_connection);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ap_auth_mode(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "dione.ap.auth", gAP_auth_mode);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_ap_channel(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_i8(my_handle, "dione.ap.channel", gAP_channel);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_save_wifi_mode(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "dione.wifimode", gWifiMode);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_uuid(uint8_t *pUUID)
{
    nvs_handle my_handle;
    memcpy(gUUID, pUUID, 16);
    memcpy(dione_raw_adv_data+9, gUUID, 16);
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.uuid", gUUID, sizeof(gUUID));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_major(uint8_t *pMajor)
{
    nvs_handle my_handle;
    memcpy(gMajor, pMajor, 2);
    memcpy(dione_raw_adv_data+25, gMajor, 2);
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.major", gMajor, sizeof(gMajor));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_minor(uint8_t *pMinor)
{
    nvs_handle my_handle;
    memcpy(gMinor, pMinor, 2);
    memcpy(dione_raw_adv_data+27, gMinor, 2);
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.minor", gMinor, sizeof(gMinor));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_tx_power_calibrated(uint8_t tx)
{
    nvs_handle my_handle;
    gTxPowerCalibrated = tx;
    dione_raw_adv_data[29] = gTxPowerCalibrated;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u8(my_handle, "dione.txcal", gTxPowerCalibrated);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_adv_interval(uint16_t interval)
{
    nvs_handle my_handle;
    g_intervalADV = (uint16_t)((float)interval * (float)100 / (float)0.625);
    dione_adv_params.adv_int_max = g_intervalADV;
    dione_adv_params.adv_int_min = g_intervalADV;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u16(my_handle, "adv_interval", g_intervalADV);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_scan_interval(uint16_t interval)
{
    nvs_handle my_handle;
    g_scan_interval = (uint16_t)((float)interval * (float)100 / (float)0.625);
    ble_scan_params.scan_interval = g_scan_interval;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u16(my_handle, "scan_interval", g_scan_interval);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_scan_window(uint16_t interval)
{
    nvs_handle my_handle;
    g_scan_window = (uint16_t)((float)interval * (float)100 / (float)0.625);
    ble_scan_params.scan_window = g_scan_window;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u16(my_handle, "scan_window", g_scan_window);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_host(uint8_t *pHost, uint8_t len)
{
    nvs_handle my_handle;
    memcpy(gDioneHost, pHost, len);
    gDioneHost[len] = 0;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.host", gDioneHost, sizeof(gDioneHost));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    strcpy(settings.host, gDioneHost);
    return ESP_OK;
}

esp_err_t dione_nvs_save_port(uint8_t *pPort)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    uint16_t port = (uint16_t)(*(pPort+1) << 8 | *pPort);
    ESP_LOGW(TAG_DIONE, "port = %d", port);
    gDionePort = port;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u32(my_handle, "dione.port", gDionePort);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    settings.port = gDionePort;
    return ESP_OK;
}

esp_err_t dione_nvs_save_ota_host(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.otahost", gDioneOTAHost, sizeof(gDioneOTAHost));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_ota_file_name(void)
{
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.file", gDioneOTAFileName, sizeof(gDioneOTAFileName));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_ota_port(void)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u32(my_handle, "dione.otaport", gDioneOTAPort);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}
esp_err_t dione_nvs_save_seq(void)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u64(my_handle, "dione.seq", gSeq);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_shake(void)
{
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    nvs_handle my_handle;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u64(my_handle, "dione.shake", gShakeCount);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_userKEY(uint8_t *pUserKey, uint8_t len)
{
    nvs_handle my_handle;
    memcpy(gUserKEY, pUserKey, len);
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "dione.key", gUserKEY, sizeof(gUserKEY));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_heart_interval(uint16_t heart_interval)
{
    nvs_handle my_handle;
    gHeartInterval = heart_interval;
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_u16(my_handle, "dione.heart", gHeartInterval);
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

esp_err_t dione_nvs_save_daps(uint8_t *pDaps, uint8_t len)
{
    nvs_handle my_handle;
    memset(gDapsCommand, 0, sizeof(gDapsCommand));
    memcpy(gDapsCommand, pDaps, len);
    gDapsCommand[len] = '\r';
    gDapsCommand[len + 1] = '\n';
    esp_err_t err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK) return err;
    err = nvs_set_blob(my_handle, "UWB.daps", gDapsCommand, sizeof(gDapsCommand));
    if (err != ESP_OK) return err;
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;
    nvs_close(my_handle);
    return ESP_OK;
}

static esp_err_t dione_nvs_init(void)
{
    size_t size;
    nvs_handle my_handle;
    esp_err_t err;
    
    err = nvs_open(DIONE_STORAGE_NAMESPACE, NVS_READONLY, &my_handle);
    if (err != ESP_OK) return err;

    uint8_t *macBT = malloc(12);

    err = nvs_get_u8(my_handle, "UWB.UM", &gHaveUM100);
    
    size = sizeof(gBLEDeviceName);
    err = nvs_get_blob(my_handle, "dione.name", gBLEDeviceName, &size);
    if (err == ESP_ERR_NVS_NOT_FOUND)//OK, let us set the default name.
    {
        esp_read_mac(macBT, ESP_MAC_BT);
        sprintf(gBLEDeviceName+5, "%02X%02X%02X%02X", macBT[2], macBT[3], macBT[4], macBT[5]);
    }
    
    //gBLEServer = 1; // Default is server.
    err = nvs_get_u8(my_handle, "ble_server", &gBLEServer);

    //g_intervalADV = 1280;
    err = nvs_get_u16(my_handle, "adv_interval", &g_intervalADV);
    dione_adv_params.adv_int_max = g_intervalADV;
    dione_adv_params.adv_int_min = g_intervalADV;

    size = sizeof(gUUID);
    err = nvs_get_blob(my_handle, "dione.uuid", gUUID, &size);
    size = sizeof(gMajor);
    err = nvs_get_blob(my_handle, "dione.major", gMajor, &size);
    size = sizeof(gMinor);
    err = nvs_get_blob(my_handle, "dione.minor", gMinor, &size);
    err = nvs_get_u8(my_handle, "dione.txcal", &gTxPowerCalibrated);
    memcpy(dione_raw_adv_data+9, gUUID, 16);
    memcpy(dione_raw_adv_data+25, gMajor, 2);
    memcpy(dione_raw_adv_data+27, gMinor, 2);
    dione_raw_adv_data[29] = gTxPowerCalibrated;

    if (gBLEServer == 0)
    {
        g_scan_interval = 160;
    }
    else
    {
        g_scan_interval = 1280;
    }
    err = nvs_get_u16(my_handle, "scan_interval", &g_scan_interval);
    ble_scan_params.scan_interval = g_scan_interval;

    if (gBLEServer == 0)
    {
        g_scan_window = 150;
    }
    else
    {
        g_scan_window = 160;
    }
    err = nvs_get_u16(my_handle, "scan_window", &g_scan_window);
    ble_scan_params.scan_window = g_scan_window;

    err = nvs_get_u8(my_handle, "dione.wifimode", &gWifiMode);

    memset(gSTA_ssid, 0, sizeof(gSTA_ssid));
    strcpy((char*)gSTA_ssid, WIFI_SSID);
    size = sizeof(gSTA_ssid);
    err = nvs_get_blob(my_handle, "dione.sta.ssid", gSTA_ssid, &size);

    memset(gSTA_pass, 0, sizeof(gSTA_pass));
    strcpy((char*)gSTA_pass, WIFI_PASS);
    size = sizeof(gSTA_pass);
    err = nvs_get_blob(my_handle, "dione.sta.pass", gSTA_pass, &size);

    memset(gAP_ssid, 0, sizeof(gAP_ssid));
    strcpy((char*)gAP_ssid, "Dione");
    size = sizeof(gAP_ssid);
    err = nvs_get_blob(my_handle, "dione.ap.ssid", gAP_ssid, &size);

    memset(gAP_pass, 0, sizeof(gAP_pass));
    strcpy((char*)gAP_pass, "123456");
    size = sizeof(gAP_pass);
    err = nvs_get_blob(my_handle, "dione.ap.pass", gAP_pass, &size);
    err = nvs_get_u8(my_handle, "dione.ap.mc", &gAP_max_connection);
    err = nvs_get_u8(my_handle, "dione.ap.auth", &gAP_auth_mode);
    err = nvs_get_i8(my_handle, "dione.ap.channel", &gAP_channel);
    if (err == ESP_ERR_NVS_NOT_FOUND)
    {
        gAP_channel = -1;
    }

#if 0
    err = nvs_get_u8(my_handle, "UWB.devicetype", &gUWBDeviceType);
    err = nvs_get_u8(my_handle, "UWB.sycj", &gUWBShiYongChangJing);
    err = nvs_get_u8(my_handle, "UWB.channel", &gUWBChannel);
    err = nvs_get_u8(my_handle, "UWB.kpm", &gUWBKuoPinMa);
    err = nvs_get_u8(my_handle, "UWB.hm", &gUWBHowMany);
    err = nvs_get_u16(my_handle, "UWB.times", &gUWBTimes);
    err = nvs_get_u16(my_handle, "UWB.interval", &gUWBInterval);
#else
    size = sizeof(gDapsCommand);
    err = nvs_get_blob(my_handle, "UWB.daps", gDapsCommand, &size);
    if (err == ESP_ERR_NVS_NOT_FOUND)
    {
        strcpy(gDapsCommand, "daps\r\n");
    }
#endif

    memset(gDioneHost, 0, sizeof(gDioneHost));
#ifdef MQTT_SERVER_DIONE
    strcpy(gDioneHost, "www.huoweiyi.com");
#elif defined(MQTT_SERVER_ONENET)
    strcpy(gDioneHost, "183.230.40.39");
#elif defined(MQTT_SERVER_MICRODUINO)
    strcpy(gDioneHost, "mcotton.microduino.cn");
#elif defined(MQTT_SERVER_SH_HUOWEI)
    strcpy(gDioneHost, "www.huoweiyi.com");
    //strcpy(gDioneHost, "61.152.175.119");
#endif
    //strcpy(gDioneHost, "112.126.75.148");
    size = sizeof(gDioneHost);
    err = nvs_get_blob(my_handle, "dione.host", gDioneHost, &size);
    err = nvs_get_u32(my_handle, "dione.port", &gDionePort);
    memset(settings.host, 0, sizeof(settings.host));
    strcpy(settings.host, gDioneHost);
    settings.port = gDionePort;


    memset(gDioneOTAHost, 0, sizeof(gDioneOTAHost));
    strcpy(gDioneOTAHost, "www.huoweiyi.com");
    size = sizeof(gDioneOTAHost);
    err = nvs_get_blob(my_handle, "dione.otahost", gDioneOTAHost, &size);
    err = nvs_get_u32(my_handle, "dione.otaport", &gDioneOTAPort);
    memset(gDioneOTAFileName, 0, sizeof(gDioneOTAFileName));
    strcpy(gDioneOTAFileName, "/DioneOKOK.bin");
    size = sizeof(gDioneOTAFileName);
    err = nvs_get_blob(my_handle, "dione.file", gDioneOTAFileName, &size);
    
    
    size = sizeof(gUserKEY);
    err = nvs_get_blob(my_handle, "dione.key", gUserKEY, &size);
    if (err == ESP_ERR_NVS_NOT_FOUND)
    {
        memset(gUserKEY, 0, sizeof(gUserKEY));
        memcpy(gUserKEY, macBT, 6);
    }

    err = nvs_get_u16(my_handle, "dione.heart", &gHeartInterval);

    err = nvs_get_u64(my_handle, "dione.seq", &gSeq);

    err = nvs_get_u64(my_handle, "dione.shake", &gShakeCount);

    err = nvs_get_u8(my_handle, "dione.mr", &gManualRestart);
    
    nvs_close(my_handle);
    free(macBT);
    return ESP_OK;
    #if 0
    if (err != ESP_OK && err != ESP_ERR_NVS_NOT_FOUND) return err;

    // Write
    g_restart_counter++;
    err = nvs_set_i32(my_handle, "restart_conter", g_restart_counter);
    if (err != ESP_OK) return err;

    // Commit written value.
    // After setting any values, nvs_commit() must be called to ensure changes are written
    // to flash storage. Implementations may write to storage at other times,
    // but this is not guaranteed.
    err = nvs_commit(my_handle);
    if (err != ESP_OK) return err;

    // Close
    nvs_close(my_handle);
    return ESP_OK;
    #endif
}

static void dione_init_leds(void *pvParameters)
{
    gpio_pad_select_gpio(LED_UWB);
    gpio_set_direction(LED_UWB, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_UWB, LED_ON);

    gpio_pad_select_gpio(LED_POWER);
    gpio_set_direction(LED_POWER, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_POWER, LED_ON);

    gpio_pad_select_gpio(LED_WIFI);
    gpio_set_direction(LED_WIFI, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_WIFI, LED_ON);

    gpio_pad_select_gpio(LED_BLE);
    gpio_set_direction(LED_BLE, GPIO_MODE_OUTPUT);
    gpio_set_level(LED_BLE, LED_ON);

    vTaskDelay(3000/portTICK_PERIOD_MS);
    gpio_set_level(LED_UWB, LED_OFF);
    gpio_set_level(LED_BLE, LED_OFF);
    gpio_set_level(LED_WIFI, LED_OFF);

    xTaskCreate(dione_BLE_led_task, "ble_led", 2048, NULL, 1, &gDioneBLE);
    
    vTaskDelete(NULL);
}

static void dione_adc_task(void* arg)
{
    //char strftime_buf[128];
    ESP_LOGW(TAG_DIONE, "Enter %s", __func__);
    // initialize ADC
    adc1_config_width(ADC_WIDTH_12Bit);
    adc1_config_channel_atten(ADC1_CHANNEL_7, ADC_ATTEN_0db);
    vTaskDelay(3000/portTICK_PERIOD_MS);
    xEventGroupWaitBits(dione_event_group, DIONE_EVENT_MQTT_CONNECTED,
                        false, false, portMAX_DELAY);
    while (true)
    {
        //printf("The adc1 value:%d\n", adc1_get_voltage(ADC1_CHANNEL_7));
        gADCvalue = adc1_get_voltage(ADC1_CHANNEL_7) + 451;
        //gVoltage = gADCvalue / 4096.0 * 5.99;
        //ESP_LOGE(TAG_DIONE, "The adc1 value:%d, voltage = %.2f\n", gADCvalue, gVoltage);
        
        //ESP_LOGW(TAG_DIONE, "%s", strftime_buf);
        #ifdef MQTT_SERVER_DIONE
        sprintf(strftime_buf, "{\"adc\":%d,\"vol\":%.2f}", gADCvalue, gVoltage);
        mqtt_publish(g_mqtt_client, "Dione/adc", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        #elif defined(MQTT_SERVER_MICRODUINO)
        sprintf(strftime_buf, "{\"adc\":%d,\"vol\":%.2f}", gADCvalue, gVoltage);
        mqtt_publish(g_mqtt_client, "dp/5936377a4eb6400001eb131a", (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        #elif defined(MQTT_SERVER_SH_HUOWEI)
        //sprintf(strftime_buf, "{\"adc\":%d,\"vol\":%.2f}", gADCvalue, gVoltage);
        //mqtt_publish(g_mqtt_client, MQTT_PUB_TOPIC_CONTROL, (char*)strftime_buf, strlen(strftime_buf), 0, 0);
        #endif
        if (gDioneState == 1)
        {
            break;
        }
        vTaskDelay(4000/portTICK_PERIOD_MS);
    }
    vTaskDelete(NULL);
}

void app_main(void)
{
    ESP_LOGW(TAG_DIONE, "Enter %s, sw version: %s", __func__, DIONE_SW_VERSION);
    ESP_LOGW(TAG_DIONE, "Free memory: %d bytes\n", esp_get_free_heap_size());
    
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES) {
        // NVS partition was truncated and needs to be erased
        const esp_partition_t* nvs_partition = esp_partition_find_first(
                ESP_PARTITION_TYPE_DATA, ESP_PARTITION_SUBTYPE_DATA_NVS, NULL);
        assert(nvs_partition && "partition table must have an NVS partition");
        ESP_ERROR_CHECK( esp_partition_erase_range(nvs_partition, 0, nvs_partition->size) );
        // Retry nvs_flash_init
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );
    dione_save_restart_counter();
    ESP_LOGW(TAG_DIONE, "boot time : %d", g_restart_counter);
    dione_nvs_init();
    ESP_LOGE(TAG_DIONE, "g_intervalADV : %d", g_intervalADV);
    
    //dione_init_leds();
    xTaskCreate(dione_init_leds, "init_led", 2048, NULL, 2, NULL);

    tcpip_adapter_init();
    dione_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(dione_wifi_event_handler, NULL) );

    if (gHaveUM100 == 1)
    {
        dione_init_uart();
        xTaskCreate(dione_uart_task, "uart_task", 4096, NULL, DIONE_TASK_PRIO_UART, &gDioneUARTTH);
    }
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        BLUFI_ERROR("%s initialize bt controller failed\n", __func__);
    }

    ret = esp_bt_controller_enable(ESP_BT_MODE_BTDM);
    if (ret) {
        BLUFI_ERROR("%s enable bt controller failed\n", __func__);
        return;
    }

    ret = esp_bluedroid_init();
    if (ret) {
        BLUFI_ERROR("%s init bluedroid failed\n", __func__);
        return;
    }

    ret = esp_bluedroid_enable();
    if (ret) {
        BLUFI_ERROR("%s init bluedroid failed\n", __func__);
        return;
    }
#if 1
    BLUFI_INFO("BD ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(esp_bt_dev_get_address()));
    BLUFI_INFO("BLUFI VERSION %04x\n", esp_blufi_get_version());
    printf("BLUFI VERSION %04x\n", esp_blufi_get_version());
    uint8_t macSTA[6];
    uint8_t macAP[6];
    uint8_t macBT[6];
    uint8_t macETH[6];
    esp_read_mac(macSTA, ESP_MAC_WIFI_STA);
    ESP_LOGW(TAG_DIONE, "STA ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macSTA));
    //printf("STA ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macSTA));
    esp_read_mac(macAP, ESP_MAC_WIFI_SOFTAP);
    ESP_LOGW(TAG_DIONE, "SOFTAP ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macAP));
    //printf("SOFTAP ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macAP));
    esp_read_mac(macBT, ESP_MAC_BT);
    ESP_LOGW(TAG_DIONE, "BT ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macBT));
    //printf("BT ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macBT));
    esp_read_mac(macETH, ESP_MAC_ETH);
    ESP_LOGW(TAG_DIONE, "ETH ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macETH));
    //printf("ETH ADDR: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(macETH));
    ESP_LOGW(TAG_DIONE, "scan_interval=%d, scan_window=%d", ble_scan_params.scan_interval, ble_scan_params.scan_window);
#endif
    sprintf(gCID, "%02X%02X%02X%02X%02X%02X", ESP_BD_ADDR_HEX(macSTA));
    ESP_LOGE(TAG_DIONE, "gCID = %s", gCID);
    strcpy(settings.client_id, gCID);

    if (gBLEServer == 2)
    {
        xTaskCreate(dione_BLE_adv_scan_task, "adv_scan", 4096, NULL, 3, &gDioneAdvScan);
    }
    esp_ble_gap_register_callback(dione_gap_event_handler);
    if (gBLEServer == 2)
    {
        blufi_security_init();    
        esp_blufi_register_callbacks(&dione_callbacks);
        esp_blufi_profile_init();
        dione_profile_init();
        esp_err_t status;
        //register the callback function to the gattc module
        if ((status = esp_ble_gattc_register_callback(dione_gattc_event_handler)) != ESP_OK) {
            ESP_LOGE(GATTC_TAG, "gattc register error, error code = %x", status);
            //return;
        }
        esp_ble_gattc_app_register(PROFILE_A_APP_ID);
    }
    else if (gBLEServer == 1) // This is a server
    {
        blufi_security_init();    
        esp_blufi_register_callbacks(&dione_callbacks);
        esp_blufi_profile_init();
#if 0
        esp_ble_gatts_register_callback(dione_gatts_event_handler);
        esp_ble_gatts_app_register(0x76);
        //esp_ble_gatts_app_register(PROFILE_B_APP_ID);
#else
        dione_profile_init();
#endif
    }
    else // This is a client.
    {
        esp_err_t status;
        
        //register the callback function to the gattc module
        if ((status = esp_ble_gattc_register_callback(dione_gattc_event_handler)) != ESP_OK) {
            ESP_LOGE(GATTC_TAG, "gattc register error, error code = %x", status);
            return;
        }
        esp_ble_gattc_app_register(PROFILE_A_APP_ID);
        //esp_ble_gattc_app_register(PROFILE_B_APP_ID);
    }
#if 0
        esp_ble_gap_set_device_name(gBLEDeviceName);
        esp_ble_gap_config_adv_data_raw(dione_raw_adv_data, sizeof(dione_raw_adv_data));
        esp_ble_gap_config_scan_rsp_data_raw(dione_raw_scan_rsp_data, sizeof(dione_raw_scan_rsp_data));
#endif
#if 0
    dione_initialise_wifi();
#else
    //dione_initialise_wifi();
    dione_initialise_ethernet();
#endif

    //bas_register();
    if (gManualRestart == 0)
    {
        xTaskCreate(dione_mqtt_rsp_config_task, "rsp_config", 2048, NULL, 11, NULL);
    }
    xTaskCreate(dione_restart_task, "restart", 2048, NULL, 10, NULL);
    xTaskCreate(dione_scan_monitor_task, "s_monitor", 2048, NULL, 10, NULL);
    xTaskCreate(dione_adc_task, "adc", 1024*3, NULL, 10, NULL);
    //xTaskCreate(dione_test_json_task, "json", 4096, NULL, 10, NULL);
    //xTaskCreate(dione_heartbeat_task, "heart", 2048, NULL, 2, &gDioneHeartBeat);
    xTaskCreate(dione_temp_task, "TEMP", 2048, NULL, 3, &gDioneTemp);
    dione_interrupt_init();
    dione_ota_init();
    //testFAT();
    getTime(NULL);
    
    ESP_LOGW(TAG_DIONE, "Leave %s", __func__);
}
