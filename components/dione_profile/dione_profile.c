
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#if 0
//#include "bluedroid_test.h"
#include "bta_api.h"
#include "bta_gatt_api.h"
#include "controller.h"
#include "gatt_int.h"
#include "bt_trace.h"
#include "btm_api.h"
#include "bt_types.h"
#include "dis_api.h"
#endif

#include "esp_system.h"
#include "esp_log.h"
#include "esp_bt_defs.h"
#include "nvs_flash.h"
#include "bt.h"
#include "bta_api.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"
#include "esp_bt_main.h"
#include "dione_profile.h"

extern uint8_t dione_raw_adv_data[];
extern uint8_t dione_raw_scan_rsp_data[];
extern uint16_t g_intervalADV;
extern uint16_t g_scan_interval;
extern uint16_t g_scan_window;
extern char gBLEDeviceName[21];
extern char gDioneHost[64];
extern uint32_t gDionePort;
extern char gUserKEY[32];
extern uint16_t gHeartInterval;
extern esp_ble_adv_params_t dione_adv_params;
extern char gDapsCommand[32];
extern uint8_t gBLEServer;

extern void dumpBytes(const uint8_t *data, size_t count);
extern esp_err_t dione_nvs_save_adv_interval(uint16_t interval);
extern esp_err_t dione_nvs_save_scan_interval(uint16_t interval);
extern esp_err_t dione_nvs_save_scan_window(uint16_t interval);
extern esp_err_t dione_nvs_save_tx_power_calibrated(uint8_t tx);
extern esp_err_t dione_nvs_save_minor(uint8_t *pMinor);
extern esp_err_t dione_nvs_save_major(uint8_t *pMajor);
extern esp_err_t dione_nvs_save_uuid(uint8_t *pUUID);
extern esp_err_t dione_nvs_save_heart_interval(uint16_t heart_interval);
extern esp_err_t dione_nvs_save_userKEY(uint8_t *pUserKey, uint8_t len);
extern esp_err_t dione_nvs_save_port(uint8_t *pPort);
extern esp_err_t dione_nvs_save_host(uint8_t *pHost, uint8_t len);
extern esp_err_t dione_nvs_save_daps(uint8_t *pDaps, uint8_t len);
extern esp_err_t dione_nvs_save_bleServer(uint8_t ble);


static void dione_resolve_command(uint8_t *data, size_t length);

#define L1_HEADER_iBEACON_MAGIC (0xEA)
#define GATTS_TABLE_TAG "Dione_Profile"
#define DIONE_PROFILE_NUM 			    1
#define DIONE_PROFILE_APP_IDX 			0
#define DIONE_PROFILE_APP_ID			0x55
//#define SAMPLE_DEVICE_NAME              "ESP_HEART_RATE"
//#define SAMPLE_MANUFACTURER_DATA_LEN    17
#define DIONE_PROFILE_SVC_INST_ID	    	0

uint16_t dione_profile_handle_table[DIONE_IDX_NB];
uint16_t dione_conn_id;               /*!< Connection id */
esp_bd_addr_t dione_remote_bda;       /*!< Remote bluetooth device address */

#if 0
#define GATTS_DEMO_CHAR_VAL_LEN_MAX		0x40
static uint8_t char1_str[] = {0x11, 0x22, 0x33};
static esp_attr_value_t gatts_demo_char1_val =
{
    .attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
    .attr_len		= sizeof(char1_str),
    .attr_value     = char1_str,
};

static uint8_t heart_rate_service_uuid[16] =
{
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0x18, 0x0D, 0x00, 0x00,
};

static esp_ble_adv_data_t heart_rate_adv_config =
{
    .set_scan_rsp = false,
    .include_name = true,
    .include_txpower = true,
    .min_interval = 0x20,
    .max_interval = 0x40,
    .appearance = 0x00,
    .manufacturer_len = 0, //TEST_MANUFACTURER_DATA_LEN,
    .p_manufacturer_data =  NULL, //&test_manufacturer[0],
    .service_data_len = 0,
    .p_service_data = NULL,
    .service_uuid_len = sizeof(heart_rate_service_uuid),
    .p_service_uuid = heart_rate_service_uuid,
    .flag = (ESP_BLE_ADV_FLAG_GEN_DISC | ESP_BLE_ADV_FLAG_BREDR_NOT_SPT),
};

static esp_ble_adv_params_t heart_rate_adv_params =
{
    .adv_int_min        = 0x20,
    .adv_int_max        = 0x40,
    .adv_type           = ADV_TYPE_IND,
    .own_addr_type      = BLE_ADDR_TYPE_PUBLIC,
    //.peer_addr            =
    //.peer_addr_type       =
    .channel_map        = ADV_CHNL_ALL,
    .adv_filter_policy = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY,
};
#endif

struct gatts_profile_inst
{
    esp_gatts_cb_t gatts_cb;
    uint16_t gatts_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_handle;
    esp_gatt_srvc_id_t service_id;
    uint16_t char_handle;
    esp_bt_uuid_t char_uuid;
    esp_gatt_perm_t perm;
    esp_gatt_char_prop_t property;
    uint16_t descr_handle;
    esp_bt_uuid_t descr_uuid;
};

static void gatts_profile_event_handler(esp_gatts_cb_event_t event,
                                        esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

/* One gatt-based profile one app_id and one gatts_if, this array will store the gatts_if returned by ESP_GATTS_REG_EVT */
static struct gatts_profile_inst dione_profile_tab[DIONE_PROFILE_NUM] =
{
    [DIONE_PROFILE_APP_IDX] = {
        .gatts_cb = gatts_profile_event_handler,
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },

};

/*
 * HTPT PROFILE ATTRIBUTES
 ****************************************************************************************
 */


/*
 *  Heart Rate PROFILE ATTRIBUTES
 ****************************************************************************************
 */

/// Heart Rate Sensor Service
static const uint16_t dione_heart_rate_svc = 0xFFA0;

#define DIONE_CHAR_DECLARATION_SIZE   (sizeof(uint8_t))
static const uint16_t dione_primary_service_uuid = ESP_GATT_UUID_PRI_SERVICE;
static const uint16_t dione_character_declaration_uuid = ESP_GATT_UUID_CHAR_DECLARE;
static const uint16_t dione_character_client_config_uuid = ESP_GATT_UUID_CHAR_CLIENT_CONFIG;
static const uint8_t dione_char_prop_notify = ESP_GATT_CHAR_PROP_BIT_NOTIFY;
static const uint8_t dione_char_prop_read = ESP_GATT_CHAR_PROP_BIT_READ;
static const uint8_t dione_char_prop_read_write = ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_READ;
static const uint8_t dione_char_prop_write = ESP_GATT_CHAR_PROP_BIT_WRITE;

/// Heart Rate Sensor Service - Heart Rate Measurement Characteristic, notify
static const uint16_t dione_heart_rate_meas_uuid = 0xFFA1;
static const uint8_t dione_heart_measurement_ccc[2] = { 0x00, 0x00};


/// Heart Rate Sensor Service -Body Sensor Location characteristic, read
static const uint16_t dione_body_sensor_location_uuid = 0xFFA2;
static const uint8_t dione_body_sensor_loc_val[20] = "Dione Hello";


/// Heart Rate Sensor Service - Heart Rate Control Point characteristic, write&read
static const uint16_t dione_rate_ctrl_point = 0xFFA3;
static const uint8_t dione_ctrl_point[20] = "Hello Dione";

/// Full DIONE Database Description - Used to add attributes into the database
static const esp_gatts_attr_db_t heart_rate_gatt_db[DIONE_IDX_NB] =
{
    // Heart Rate Service Declaration
    [DIONE_IDX_SVC]                      	=
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_primary_service_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            sizeof(uint16_t), sizeof(dione_heart_rate_svc), (uint8_t *) &dione_heart_rate_svc
        }
    },

    // Heart Rate Measurement Characteristic Declaration
    [DIONE_IDX_HR_MEAS_CHAR]            =
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_character_declaration_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            DIONE_CHAR_DECLARATION_SIZE, DIONE_CHAR_DECLARATION_SIZE, (uint8_t *) &dione_char_prop_notify
        }
    },

    // Heart Rate Measurement Characteristic Value
    [DIONE_IDX_HR_MEAS_VAL]             	=
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_heart_rate_meas_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            DIONE_HT_MEAS_MAX_LEN, 0, NULL
        }
    },

    // Heart Rate Measurement Characteristic - Client Characteristic Configuration Descriptor
    [DIONE_IDX_HR_MEAS_NTF_CFG]     	=
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_character_client_config_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            sizeof(uint16_t), sizeof(dione_heart_measurement_ccc), (uint8_t *)dione_heart_measurement_ccc
        }
    },

    // Body Sensor Location Characteristic Declaration
    [DIONE_IDX_BOBY_SENSOR_LOC_CHAR]  =
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_character_declaration_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            DIONE_CHAR_DECLARATION_SIZE, DIONE_CHAR_DECLARATION_SIZE, (uint8_t *) &dione_char_prop_write
        }
    },

    // Body Sensor Location Characteristic Value
    [DIONE_IDX_BOBY_SENSOR_LOC_VAL]   =
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_body_sensor_location_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
            sizeof(dione_body_sensor_loc_val), sizeof(dione_body_sensor_loc_val), (uint8_t *)dione_body_sensor_loc_val
        }
    },
#if 0
    // Heart Rate Control Point Characteristic Declaration
    [DIONE_IDX_HR_CTNL_PT_CHAR]          =
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_character_declaration_uuid, ESP_GATT_PERM_READ,
            DIONE_CHAR_DECLARATION_SIZE, DIONE_CHAR_DECLARATION_SIZE, (uint8_t *) &dione_char_prop_write
        }
    },

    // Heart Rate Control Point Characteristic Value
    [DIONE_IDX_HR_CTNL_PT_VAL]             =
    {   {ESP_GATT_AUTO_RSP}, {
            ESP_UUID_LEN_16, (uint8_t *) &dione_rate_ctrl_point, ESP_GATT_PERM_WRITE | ESP_GATT_PERM_READ,
            sizeof(dione_ctrl_point), sizeof(dione_ctrl_point), (uint8_t *)dione_ctrl_point
        }
    },
#endif
};

#if 0
static void dione_profile_gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    ESP_LOGE(GATTS_TABLE_TAG, "GAP_EVT, event %d\n", event);

    switch (event)
    {
    case ESP_GAP_BLE_ADV_DATA_SET_COMPLETE_EVT:
        esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
        //advertising start complete event to indicate advertising start successfully or failed
        if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS)
        {
            ESP_LOGE(GATTS_TABLE_TAG, "Advertising start failed\n");
        }
        break;
    default:
        break;
    }
}
#endif

static void gatts_profile_event_handler(esp_gatts_cb_event_t event,
                                        esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    ESP_LOGE(GATTS_TABLE_TAG, "event = %x\n", event);
    switch (event)
    {
    case ESP_GATTS_REG_EVT:
        esp_ble_gap_set_device_name(gBLEDeviceName);
        memcpy(dione_raw_scan_rsp_data+7, gBLEDeviceName+5, 8);
        //esp_ble_gap_config_adv_data(&heart_rate_adv_config);
        esp_ble_gap_config_adv_data_raw(dione_raw_adv_data, 31);
        esp_ble_gap_config_scan_rsp_data_raw(dione_raw_scan_rsp_data, 18);
        esp_ble_gatts_create_attr_tab(heart_rate_gatt_db, gatts_if,
                                      DIONE_IDX_NB, DIONE_PROFILE_SVC_INST_ID);
        break;
    case ESP_GATTS_READ_EVT:
        break;
    case ESP_GATTS_WRITE_EVT:
    {
        ESP_LOGI(GATTS_TABLE_TAG, "GATT_WRITE_EVT, conn_id %d, trans_id %d, handle %d\n", param->write.conn_id, param->write.trans_id, param->write.handle);
        ESP_LOGI(GATTS_TABLE_TAG, "GATT_WRITE_EVT, value len %d, value %08x\n", param->write.len, *(uint32_t *)param->write.value);
        dumpBytes(param->write.value, param->write.len);
        if (param->write.handle == dione_profile_handle_table[DIONE_IDX_BOBY_SENSOR_LOC_VAL])
        {
            dione_resolve_command(param->write.value, param->write.len);
        }
        else
        {
        }
        break;
    }
    case ESP_GATTS_CREATE_EVT:
        break;
    case ESP_GATTS_ADD_CHAR_EVT:
        break;
    case ESP_GATTS_ADD_CHAR_DESCR_EVT:
        break;
    case ESP_GATTS_EXEC_WRITE_EVT:
        break;
    case ESP_GATTS_MTU_EVT:
        ESP_LOGI(GATTS_TABLE_TAG, "conn_id = %d, mtu size = %d", param->mtu.conn_id, param->mtu.mtu);
        break;
    case ESP_GATTS_CONF_EVT:
        ESP_LOGI(GATTS_TABLE_TAG, "conn_id = %d, status = 0x%x\n", param->conf.conn_id, param->conf.status);
        break;
    case ESP_GATTS_UNREG_EVT:
        break;
    case ESP_GATTS_DELETE_EVT:
        break;
    case ESP_GATTS_START_EVT:
        break;
    case ESP_GATTS_STOP_EVT:
        break;
    case ESP_GATTS_CONNECT_EVT:
        dione_conn_id = param->connect.conn_id;
        memcpy(dione_remote_bda, param->connect.remote_bda, sizeof(dione_remote_bda));
        ESP_LOGI(GATTS_TABLE_TAG, "dione_conn_id = %d", dione_conn_id);
        ESP_LOGI(GATTS_TABLE_TAG, "remote bda: "ESP_BD_ADDR_STR"\n", ESP_BD_ADDR_HEX(dione_remote_bda));
        break;
    case ESP_GATTS_DISCONNECT_EVT:
        esp_ble_gap_start_advertising(&dione_adv_params);
        break;
    case ESP_GATTS_OPEN_EVT:
        break;
    case ESP_GATTS_CANCEL_OPEN_EVT:
        break;
    case ESP_GATTS_CLOSE_EVT:
        break;
    case ESP_GATTS_LISTEN_EVT:
        break;
    case ESP_GATTS_CONGEST_EVT:
        break;
    case ESP_GATTS_CREAT_ATTR_TAB_EVT:
{
    ESP_LOGE(GATTS_TABLE_TAG, "The number handle = %d\n", param->add_attr_tab.num_handle);
    if(param->add_attr_tab.num_handle == DIONE_IDX_NB)
    {
        memcpy(dione_profile_handle_table, param->add_attr_tab.handles,
               sizeof(dione_profile_handle_table));
        esp_ble_gatts_start_service(dione_profile_handle_table[DIONE_IDX_SVC]);
        ESP_LOGI(GATTS_TABLE_TAG, "handles -- %d:%d:%d:%d:%d:%d", ESP_BD_ADDR_HEX(dione_profile_handle_table));
    }
    break;
}

    default:
        break;
    }
}


void dione_send_notify(uint8_t *pData, uint8_t len)
{
    ESP_LOGW(GATTS_TABLE_TAG, "Enter %s(), len = %d\n", __func__, len);
    //esp_ble_gatts_hdl_val_indica(conn_id, p_inst->ba_level_hdl, 1, &battery_level, false);
    //esp_ble_gatts_hdl_val_indica(dione_conn_id, dione_profile_handle_table[DIONE_IDX_BOBY_SENSOR_LOC_VAL], len, pData, false);
    ESP_LOGW(GATTS_TABLE_TAG, "gatts_if = %d, conn_id = %d, handle = %d", dione_profile_tab[DIONE_PROFILE_APP_IDX].gatts_if,
                                                            dione_conn_id,dione_profile_handle_table[DIONE_IDX_HR_MEAS_VAL]);
    esp_ble_gatts_send_indicate(dione_profile_tab[DIONE_PROFILE_APP_IDX].gatts_if, 
                                dione_conn_id, 
                                dione_profile_handle_table[DIONE_IDX_HR_MEAS_VAL],
                                len, pData, false);
}

static bool check_length(size_t length)
{
    uint8_t buf[12] = {0};
    if (length != 4)
    {
        strcpy((char*)buf, "FAIL");
        dione_send_notify(buf, 4);
        return false;
    }
    return true;
}

static void resolve_server_command(uint8_t *pBuf, size_t length)
{
    uint8_t buf[128] = {0};
    switch (pBuf[2]) //key value
    {
        case 0x01://设置后台IP地址
        {
            uint8_t len = pBuf[3];
            if (len > 63)
            {
                strcpy((char*)buf, "FAIL:check length");
                dione_send_notify(buf, 17);
                break;
            }
            dione_nvs_save_host(pBuf+4, len);
            strcpy((char*)buf, "OK");
            dione_send_notify(buf, 2);
        }
        break;
        
        case 0x02://读取后台IP地址
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x02;
            buf[3] = strnlen(gDioneHost, sizeof(gDioneHost));
            memcpy(buf + 4, gDioneHost, buf[3]);
            dione_send_notify(buf, 4+buf[3]);
        }
        break;

        case 0x03://设置端口
        {
            if (pBuf[3] != 2)
            {
                strcpy((char*)buf, "FAIL:check length");
                dione_send_notify(buf, 17);
                break;
            }
            dione_nvs_save_port(pBuf+4);
            strcpy((char*)buf, "OK");
            dione_send_notify(buf, 2);
        }
        break;

        case 0x04://读取端口
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x04;
            buf[3] = 2;
            uint16_t port = gDionePort;
            memcpy(buf + 4, &port, 2);
            dione_send_notify(buf, 6);
        }
        break;

        case 0x05://设置UserKEY
        {
            if (pBuf[3] != 16)
            {
                strcpy((char*)buf, "FAIL:check length");
                dione_send_notify(buf, 17);
                break;
            }
            dione_nvs_save_userKEY(pBuf+4, pBuf[3]);
            strcpy((char*)buf, "OK");
            dione_send_notify(buf, 2);
        }
        break;

        case 0x06://读取UserKEY
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x06;
            buf[3] = 16;
            memcpy(buf+4, gUserKEY, 16);
            dione_send_notify(buf, 20);
        }
        break;

        case 0x07://设置心跳频度
        {
            if (pBuf[3] != 1)
            {
                strcpy((char*)buf, "FAIL:check length");
                dione_send_notify(buf, 17);
                break;
            }
            dione_nvs_save_heart_interval(pBuf[4]);
            strcpy((char*)buf, "OK");
            dione_send_notify(buf, 2);
        }
        break;

        case 0x08://读取心跳频度
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x08;
            buf[3] = 1;
            buf[4] = gHeartInterval;
            dione_send_notify(buf, 5);
        }
        break;
            
        default:
            break;
    }
}

static void resolve_daps_command(uint8_t *pBuf, size_t length)
{
    uint8_t buf[128] = {0};
    switch (pBuf[2]) //key value
    {
        case 0x01://设置新的daps指令
        {
            uint8_t len = pBuf[3];
            if (len > 27)
            {
                strcpy((char*)buf, "FAIL:check length");
                dione_send_notify(buf, 17);
                break;
            }
            dione_nvs_save_daps(pBuf+4, len);
            strcpy((char*)buf, "OK");
            dione_send_notify(buf, 2);
        }
        break;
        
        case 0x02://读取当前daps指令
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x02;
            buf[3] = strnlen(gDapsCommand, sizeof(gDapsCommand)) - 2;
            
            //buf[3] = strlen(gDapsCommand);
            memcpy(buf + 4, gDapsCommand, buf[3]);
            dione_send_notify(buf, 4 + buf[3]);
        }
        break;

        default:
            break;
    }
}

static void dione_ble_reset_task(void *pvParameters)
{
    ESP_LOGI(GATTS_TABLE_TAG, "Enter %s", __func__);
    uint8_t buf[32] = {0};
    buf[0] = L1_HEADER_iBEACON_MAGIC;
    buf[1] = 0x08;
    buf[2] = 0x01;
    buf[3] = 1;
    for (int i = 4; i >= 0; i--) 
    {
        buf[4] = i;
        dione_send_notify(buf, 5);
        vTaskDelay(1000 / portTICK_PERIOD_MS);
    }
    esp_restart();
}

static void resolve_Dione_system_command(uint8_t *pBuf, size_t length)
{
    uint8_t buf[128] = {0};
    uint8_t len = pBuf[3];
    if (len != 0)
    {
        strcpy((char*)buf, "FAIL");
        dione_send_notify(buf, 4);
        return;
    }
    switch (pBuf[2]) //key value
    {
        case 0x01://重启系统以使新的配置生效
        {
            if (check_length(length) == false) break;
            xTaskCreate(dione_ble_reset_task, "ble_reset", 2048, NULL, 25, NULL);
        }
        break;
        
        case 0x02://读取蓝牙mac地址
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x02;
            buf[3] = 6;
            esp_read_mac(buf + 4, ESP_MAC_BT);
            dione_send_notify(buf, 10);
        }
        break;
        case 0x03://读取STA mac地址
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x03;
            buf[3] = 6;
            esp_read_mac(buf + 4, ESP_MAC_WIFI_STA);
            dione_send_notify(buf, 10);
        }
        break;
        case 0x04://读取SOFT AP mac地址
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x04;
            buf[3] = 6;
            esp_read_mac(buf + 4, ESP_MAC_WIFI_SOFTAP);
            dione_send_notify(buf, 10);
        }
        break;
        case 0x05://读取Ethernet mac地址
        {
            if (check_length(length) == false) break;
            buf[0] = L1_HEADER_iBEACON_MAGIC;
            buf[1] = pBuf[1];
            buf[2] = 0x05;
            buf[3] = 6;
            esp_read_mac(buf + 4, ESP_MAC_ETH);
            dione_send_notify(buf, 10);
        }
        break;

        default:
            break;
    }
}

static void dione_resolve_command(uint8_t* data, size_t length)
{
    ESP_LOGI(GATTS_TABLE_TAG, "Enter %s, length = %d", __func__, length);
    if (data[0] != L1_HEADER_iBEACON_MAGIC) 
    {
        ESP_LOGI(GATTS_TABLE_TAG, "Not command , so just return.");
        return;
    }
    uint8_t buf[64] = {0};
    uint8_t len = data[3];
    switch (data[1]) //Command ID
    {
        case 0x00: { //蓝牙工作模式指令
            if (data[2] == 0x01)//set
            {
                if (len != 1)
                {
                    strcpy((char*)buf, "FAIL:check UUID len");
                    dione_send_notify(buf, 19);
                    break;
                }
                if (data[4] != 0x00 && data[4] != 0x01)
                {
                    strcpy((char*)buf, "FAIL:Wrong value");
                    dione_send_notify(buf, 16);
                    break;
                }
                dione_nvs_save_bleServer(data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 1;
                buf[4] = gBLEServer;
                dione_send_notify(buf, 5);
            }
            break;
        }
        case 0x01://UUID
        {
            if (data[2] == 0x01)//set
            {
                if (len != 16)
                {
                    strcpy((char*)buf, "FAIL:check UUID len");
                    dione_send_notify(buf, 19);
                    break;
                }
                dione_nvs_save_uuid(&data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 16;
                memcpy(buf + 4, dione_raw_adv_data+9, 16);
                dione_send_notify(buf, 20);
            }
        }
        break;

        case 0x02://major
        {
            if (data[2] == 0x01)//set
            {
                if (len != 2)
                {
                    strcpy((char*)buf, "FAIL:check major len");
                    dione_send_notify(buf, 20);
                    break;
                }
                dione_nvs_save_major(&data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 2;
                memcpy(buf + 4, dione_raw_adv_data + 25, 2);
                dione_send_notify(buf, 6);
            }
        }
        break;
        case 0x03://minor
        {
            if (data[2] == 0x01)//set
            {
                if (len != 2)
                {
                    strcpy((char*)buf, "FAIL:check minor len");
                    dione_send_notify(buf, 20);
                    break;
                }
                dione_nvs_save_minor(&data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 2;
                memcpy(buf+4, dione_raw_adv_data+27, 2);
                dione_send_notify(buf, 6);
            }
        }
        break;
        case 0x04://The 2's complement of the calibrated Tx Power
        {
            if (data[2] == 0x01)//set
            {
                if (len != 1)
                {
                    strcpy((char*)buf, "FAIL:check length");
                    dione_send_notify(buf, 17);
                    break;
                }
                dione_nvs_save_tx_power_calibrated(data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 1;
                memcpy(buf+4, dione_raw_adv_data+29, 1);
                dione_send_notify(buf, 5);
            }
        }
        break;
        case 0x05://Adv&Scan interval
        {
            if (data[2] == 0x01)//set adv interval
            {
                if (len != 1)
                {
                    strcpy((char*)buf, "FAIL:check length");
                    dione_send_notify(buf, 17);
                    break;
                }
                if (data[4] > 102)
                {
                    strcpy((char*)buf, "FAIL:Too big!");
                    dione_send_notify(buf, 13);
                    break;
                }
                if (data[4] == 0)
                {
                    strcpy((char*)buf, "FAIL:Zero!");
                    dione_send_notify(buf, 10);
                    break;
                }
                dione_nvs_save_adv_interval(data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x02)//read adv interval
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x02;
                buf[3] = 1;
                buf[4] = (uint8_t)((float)g_intervalADV * (float)0.625 / (float)100);
                dione_send_notify(buf, 5);
            }
            else if (data[2] == 0x03)//set scan interval
            {
                if (len != 1)
                {
                    strcpy((char*)buf, "FAIL:check length");
                    dione_send_notify(buf, 17);
                    break;
                }
                if (data[4] > 102)
                {
                    strcpy((char*)buf, "FAIL:Too big!");
                    dione_send_notify(buf, 13);
                    break;
                }
                if (data[4] == 0)
                {
                    strcpy((char*)buf, "FAIL:Zero!");
                    dione_send_notify(buf, 10);
                    break;
                }
                dione_nvs_save_scan_interval(data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x04)//read scan interval
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x04;
                buf[3] = 1;
                buf[4] = (uint8_t)((float)g_scan_interval * (float)0.625 / (float)100);
                dione_send_notify(buf, 5);
            }
            else if (data[2] == 0x05)//set scan window
            {
                if (len != 1)
                {
                    strcpy((char*)buf, "FAIL:check length");
                    dione_send_notify(buf, 17);
                    break;
                }
                if (data[4] > 102)
                {
                    strcpy((char*)buf, "FAIL:Too big!");
                    dione_send_notify(buf, 13);
                    break;
                }
                if (data[4] == 0)
                {
                    strcpy((char*)buf, "FAIL:Zero!");
                    dione_send_notify(buf, 10);
                    break;
                }
                dione_nvs_save_scan_window(data[4]);
                strcpy((char*)buf, "OK");
                dione_send_notify(buf, 2);
            }
            else if (data[2] == 0x06)//read scan window
            {
                if (check_length(length) == false) break;
                buf[0] = L1_HEADER_iBEACON_MAGIC;
                buf[1] = data[1];
                buf[2] = 0x06;
                buf[3] = 1;
                buf[4] = (uint8_t)((float)g_scan_window * (float)0.625 / (float)100);
                dione_send_notify(buf, 5);
            }
        }
        break;
        case 0x06: {
            resolve_server_command(data, length);
        }
        break;
        case 0x07: {
            resolve_daps_command(data, length);
        }
        break;
        case 0x08:
            resolve_Dione_system_command(data, length);
            break;
        default:
            strcpy((char*)buf, "Who are you?");
            dione_send_notify(buf, 12);
            break;
    }
}

static void dione_profile_gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if,
        esp_ble_gatts_cb_param_t *param)
{
    ESP_LOGI(GATTS_TABLE_TAG, "EVT %d, gatts if %d\n", event, gatts_if);

    /* If event is register event, store the gatts_if for each profile */
    if (event == ESP_GATTS_REG_EVT)
    {
        if (param->reg.status == ESP_GATT_OK)
        {
            dione_profile_tab[DIONE_PROFILE_APP_IDX].gatts_if = gatts_if;
        }
        else
        {
            ESP_LOGI(GATTS_TABLE_TAG, "Reg app failed, app_id %04x, status %d\n",
                     param->reg.app_id,
                     param->reg.status);
            return;
        }
    }

    do
    {
        int idx;
        for (idx = 0; idx < DIONE_PROFILE_NUM; idx++)
        {
            if (gatts_if == ESP_GATT_IF_NONE || /* ESP_GATT_IF_NONE, not specify a certain gatt_if, need to call every profile cb function */
                    gatts_if == dione_profile_tab[idx].gatts_if)
            {
                if (dione_profile_tab[idx].gatts_cb)
                {
                    dione_profile_tab[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    }
    while (0);
}

void dione_profile_init(void)
{
    esp_ble_gatts_register_callback(dione_profile_gatts_event_handler);
    //esp_ble_gap_register_callback(dione_gap_event_handler);
    esp_ble_gatts_app_register(DIONE_PROFILE_APP_ID);
}
