#ifndef DIONE_MQTT_MSG_H
#define DIONE_MQTT_MSG_H

#ifdef  __cplusplus
extern "C" {
#endif

/*
* Copyright (c) 2014, Stephen Robinson
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions
* are met:
*
* 1. Redistributions of source code must retain the above copyright
* notice, this list of conditions and the following disclaimer.
* 2. Redistributions in binary form must reproduce the above copyright
* notice, this list of conditions and the following disclaimer in the
* documentation and/or other materials provided with the distribution.
* 3. Neither the name of the copyright holder nor the names of its
* contributors may be used to endorse or promote products derived
* from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/
/* 7      6     5     4     3     2     1     0 */
/*|      --- Message Type----     |  DUP Flag |    QoS Level    | Retain  | */
/*                    Remaining Length                 */


enum dione_mqtt_message_type
{
  DIONE_MQTT_MSG_TYPE_CONNECT = 1,
  DIONE_MQTT_MSG_TYPE_CONNACK = 2,
  DIONE_MQTT_MSG_TYPE_PUBLISH = 3,
  DIONE_MQTT_MSG_TYPE_PUBACK = 4,
  DIONE_MQTT_MSG_TYPE_PUBREC = 5,
  DIONE_MQTT_MSG_TYPE_PUBREL = 6,
  DIONE_MQTT_MSG_TYPE_PUBCOMP = 7,
  DIONE_MQTT_MSG_TYPE_SUBSCRIBE = 8,
  DIONE_MQTT_MSG_TYPE_SUBACK = 9,
  DIONE_MQTT_MSG_TYPE_UNSUBSCRIBE = 10,
  DIONE_MQTT_MSG_TYPE_UNSUBACK = 11,
  DIONE_MQTT_MSG_TYPE_PINGREQ = 12,
  DIONE_MQTT_MSG_TYPE_PINGRESP = 13,
  DIONE_MQTT_MSG_TYPE_DISCONNECT = 14
};

typedef struct dione_mqtt_message
{
  uint8_t* data;
  uint16_t length;

} dione_mqtt_message_t;

typedef struct dione_mqtt_connection
{
  dione_mqtt_message_t message;

  uint16_t message_id;
  uint8_t* buffer;
  uint16_t buffer_length;

} dione_mqtt_connection_t;

typedef struct dione_mqtt_connect_info
{
  char* client_id;
  char* username;
  char* password;
  char* will_topic;
  char* will_message;
  int keepalive;
  int will_qos;
  int will_retain;
  int clean_session;

} dione_mqtt_connect_info_t;


static inline int dione_mqtt_get_type(uint8_t* buffer) { return (buffer[0] & 0xf0) >> 4; }
static inline int dione_mqtt_get_connect_return_code(uint8_t* buffer) { return buffer[3]; }
static inline int dione_mqtt_get_dup(uint8_t* buffer) { return (buffer[0] & 0x08) >> 3; }
static inline int dione_mqtt_get_qos(uint8_t* buffer) { return (buffer[0] & 0x06) >> 1; }
static inline int dione_mqtt_get_retain(uint8_t* buffer) { return (buffer[0] & 0x01); }

void dione_mqtt_msg_init(dione_mqtt_connection_t* connection, uint8_t* buffer, uint16_t buffer_length);
int dione_mqtt_get_total_length(uint8_t* buffer, uint16_t length);
const char* dione_mqtt_get_publish_topic(uint8_t* buffer, uint16_t* length);
const char* dione_mqtt_get_publish_data(uint8_t* buffer, uint16_t* length);
uint16_t dione_mqtt_get_id(uint8_t* buffer, uint16_t length);

dione_mqtt_message_t* dione_mqtt_msg_connect(dione_mqtt_connection_t* connection, dione_mqtt_connect_info_t* info);
dione_mqtt_message_t* dione_mqtt_msg_publish(dione_mqtt_connection_t* connection, const char* topic, const char* data, int data_length, int qos, int retain, uint16_t* message_id);
dione_mqtt_message_t* dione_mqtt_msg_puback(dione_mqtt_connection_t* connection, uint16_t message_id);
dione_mqtt_message_t* dione_mqtt_msg_pubrec(dione_mqtt_connection_t* connection, uint16_t message_id);
dione_mqtt_message_t* dione_mqtt_msg_pubrel(dione_mqtt_connection_t* connection, uint16_t message_id);
dione_mqtt_message_t* dione_mqtt_msg_pubcomp(dione_mqtt_connection_t* connection, uint16_t message_id);
dione_mqtt_message_t* dione_mqtt_msg_subscribe(dione_mqtt_connection_t* connection, const char* topic, int qos, uint16_t* message_id);
dione_mqtt_message_t* dione_mqtt_msg_unsubscribe(dione_mqtt_connection_t* connection, const char* topic, uint16_t* message_id);
dione_mqtt_message_t* dione_mqtt_msg_pingreq(dione_mqtt_connection_t* connection);
dione_mqtt_message_t* dione_mqtt_msg_pingresp(dione_mqtt_connection_t* connection);
dione_mqtt_message_t* dione_mqtt_msg_disconnect(dione_mqtt_connection_t* connection);


#ifdef  __cplusplus
}
#endif

#endif  /* MQTT_MSG_H */

